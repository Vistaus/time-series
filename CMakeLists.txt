cmake_minimum_required(VERSION 3.0.0)
project(time-series C CXX)

# Automatically create moc files
set(CMAKE_AUTOMOC ON)

find_package(Qt5Core REQUIRED)
find_package(Qt5Qml REQUIRED)
find_package(Qt5Quick REQUIRED)
find_package(Qt5QuickControls2 REQUIRED)

execute_process(
    COMMAND dpkg-architecture -qDEB_HOST_MULTIARCH
    OUTPUT_VARIABLE ARCH_TRIPLET
    OUTPUT_STRIP_TRAILING_WHITESPACE
)
set(QT_IMPORTS_DIR "lib/${ARCH_TRIPLET}")

set(PROJECT_NAME "time-series")
set(FULL_PROJECT_NAME "time-series.matdahl")
set(DATA_DIR /)
set(DESKTOP_FILE_NAME ${PROJECT_NAME}.desktop)

# This command figures out the minimum SDK framework for use in the manifest
# file via the environment variable provided by Clickable or sets a default value otherwise.
if(DEFINED ENV{SDK_FRAMEWORK})
    set(CLICK_FRAMEWORK "$ENV{SDK_FRAMEWORK}")
else()
    set(CLICK_FRAMEWORK "ubuntu-sdk-20.04")
endif()

# This figures out the target architecture for use in the manifest file.
if(DEFINED ENV{ARCH})
    set(CLICK_ARCH "$ENV{ARCH}")
else()
    execute_process(
        COMMAND dpkg-architecture -qDEB_HOST_ARCH
        OUTPUT_VARIABLE CLICK_ARCH
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
endif()

# Sets BUILD_VERSION: Either tag of the current git HEAD or devel build version with git hash
execute_process(
  COMMAND bash "-c" "git describe --tags --abbrev=0 --exact-match | cut -d \"-\" -f 1"
  OUTPUT_VARIABLE BUILD_VERSION_RAW
  OUTPUT_STRIP_TRAILING_WHITESPACE
  ERROR_QUIET
  )
if(NOT BUILD_VERSION_RAW)
  execute_process(
    COMMAND git describe --tags --abbrev=0
    OUTPUT_VARIABLE LAST_VERSION_RAW
    OUTPUT_STRIP_TRAILING_WHITESPACE
    ERROR_QUIET
    )
  string(TIMESTAMP BUILD_VERSION_RAW "${LAST_VERSION_RAW}.%y%m%d%H%M%S" UTC)
endif(NOT BUILD_VERSION_RAW)
string(SUBSTRING ${BUILD_VERSION_RAW} 1 -1 BUILD_VERSION)
message(STATUS "Build version is: ${BUILD_VERSION}")


configure_file(manifest.json.in ${CMAKE_CURRENT_BINARY_DIR}/manifest.json)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/manifest.json DESTINATION ${CMAKE_INSTALL_PREFIX})
install(FILES ${PROJECT_NAME}.apparmor DESTINATION ${DATA_DIR})
install(FILES assets/logo.svg DESTINATION assets)


# generate QML resource file
set(QML_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/qml)
set(QML_QRC ${QML_SRC_DIR}/qml.qrc)
file(GLOB_RECURSE QML_FILES RELATIVE ${QML_SRC_DIR} qml/*.qml)
execute_process(
    COMMAND bash -c "pushd qml > /dev/null; \
                     echo \"<RCC>\" > ${QML_QRC}; \
                     echo \"  <qresource prefix=\\\"/qml\\\">\" >> ${QML_QRC}; \
                     for f in $(echo \"${QML_FILES}\" | sed 's/;/ /g'); \
                     do \
                         echo \"    <file>$f</file>\" >> ${QML_QRC}; \
                     done; \
                     echo \"  </qresource>\" >> ${QML_QRC}; \
                     echo \"</RCC>\" >> ${QML_QRC}; \
                     popd > /dev/null"
)
message(STATUS "Created QML QRC file at: ${QML_QRC}")
qt5_add_resources(QT_RESOURCES ${QML_QRC})


# generate assets resource file
set(ASSETS_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/assets)
set(ASSETS_QRC ${ASSETS_SRC_DIR}/assets.qrc)
file(GLOB_RECURSE ASSETS_FILES RELATIVE ${ASSETS_SRC_DIR} assets/*.svg)
execute_process(
    COMMAND bash -c "pushd assets > /dev/null; \
                     echo \"<RCC>\" > ${ASSETS_QRC}; \
                     echo \"  <qresource prefix=\\\"/assets\\\">\" >> ${ASSETS_QRC}; \
                     for f in $(echo \"${ASSETS_FILES}\" | sed 's/;/ /g'); \
                     do \
                         echo \"    <file>$f</file>\" >> ${ASSETS_QRC}; \
                     done; \
                     echo \"  </qresource>\" >> ${ASSETS_QRC}; \
                     echo \"</RCC>\" >> ${ASSETS_QRC}; \
                     popd > /dev/null"
)
message(STATUS "Created assets QRC file at: ${ASSETS_QRC}")
qt5_add_resources(QT_RESOURCES ${ASSETS_QRC})

add_executable(${PROJECT_NAME} main.cpp ${QT_RESOURCES})
set_target_properties(${PROJECT_NAME} PROPERTIES LINK_FLAGS_RELEASE -s)
target_link_libraries(${PROJECT_NAME} Qt5::Gui Qt5::Qml Qt5::Quick Qt5::QuickControls2)
install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX})

# Translations
file(GLOB_RECURSE I18N_SRC_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/po qml/*.qml qml/*.js)
list(APPEND I18N_SRC_FILES ${DESKTOP_FILE_NAME}.in.h)

find_program(INTLTOOL_MERGE intltool-merge)
if(NOT INTLTOOL_MERGE)
    message(FATAL_ERROR "Could not find intltool-merge, please install the intltool package")
endif()
find_program(INTLTOOL_EXTRACT intltool-extract)
if(NOT INTLTOOL_EXTRACT)
    message(FATAL_ERROR "Could not find intltool-extract, please install the intltool package")
endif()

add_custom_target(${DESKTOP_FILE_NAME} ALL
    COMMENT "Merging translations into ${DESKTOP_FILE_NAME}..."
    COMMAND LC_ALL=C ${INTLTOOL_MERGE} -d -u ${CMAKE_SOURCE_DIR}/po ${CMAKE_SOURCE_DIR}/${DESKTOP_FILE_NAME}.in ${DESKTOP_FILE_NAME}
    COMMAND sed -i 's/${PROJECT_NAME}-//g' ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE_NAME}
)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE_NAME} DESTINATION ${DATA_DIR})

add_subdirectory(po)
add_subdirectory(src)

if(NOT ${CMAKE_BUILD_TYPE} MATCHES "Release")
    message(STATUS "Add tests ...")
    add_subdirectory(tests)
else()
    message(STATUS "Skip tests ...")
endif()

# include MDToolKit
add_subdirectory(libs/mdtoolkit)

# Make source files visible in qtcreator
file(GLOB_RECURSE PROJECT_SRC_FILES
    RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
    assets/*.svg
    qml/*.qml
    src/*
    *.json
    *.json.in
    *.yaml
    *.yaml.in
    *.apparmor
    *.desktop.in
    po/*.pot
)

add_custom_target(${PROJECT_NAME}_FILES ALL SOURCES ${PROJECT_SRC_FILES})
