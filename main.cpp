/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QCoreApplication>
#include <QUrl>
#include <QDir>
#include <QString>
#include <QStandardPaths>
#include <QQuickView>

int main(int argc, char *argv[])
{
    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
    app->setApplicationName("time-series.matdahl");

    qInfo() << "#################################";
    qInfo() << "##### Starting TIME SERIES! #####";
    qInfo() << "#################################";
    qDebug() << "Data path is:   " << QStandardPaths::writableLocation(QStandardPaths::DataLocation);
    qDebug() << "Config path is: " << QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);

    // ensure data dirs exists
    QDir().mkpath(QStandardPaths::writableLocation(QStandardPaths::DataLocation));

    QQuickView *view = new QQuickView();
    view->setSource(QUrl("qrc:/qml/Main.qml"));
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();

    return app->exec();
}
