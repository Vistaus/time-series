find_package(Qt5Core)

set(UNITNAME "datacolumnmodel")
set(TARGETNAME "unittests_${UNITNAME}")

file(GLOB SRC unittests_${UNITNAME}.cpp
    ../../../src/TSCore/timeseries/datacolumn.cpp
    ../../../src/TSCore/timeseries/datacolumnmodel.cpp
)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../../../src/TSCore)
add_executable(${TARGETNAME} test_main.cpp ${SRC})
qt5_use_modules(${TARGETNAME} Core Test)
message(STATUS "Unit test \"${UNITNAME}\" added.")
