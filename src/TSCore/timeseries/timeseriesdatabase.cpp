/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timeseriesdatabase.h"

TimeseriesDatabase::TimeseriesDatabase(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",m_db_name);
    db.setDatabaseName(QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "/" + m_db_name + ".sqlite");
    if (!db.open())
        qCritical() << "TimeseriesDatabase::TimeseriesDatabase(): Could not open database: " << db.lastError().text();

    // init master data table
    QSqlQuery query("CREATE TABLE IF NOT EXISTS timeseries ("
                    "uid INTEGER PRIMARY KEY,"
                    "rank INTEGER DEFAULT 0,"
                    "title TEXT DEFAULT '')"
                    ,db);
    if (query.lastError().type())
        qCritical() << "TimeSeriesDatabase::TimeSeriesDatabase(): Could not create table 'timeseries': " << query.lastError().text();
}

bool TimeseriesDatabase::read(QList<Timeseries *> &list, QObject *parent){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "TimeseriesDatabase::read(): Could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query("SELECT * FROM timeseries",db);
    if (query.lastError().type()){
        qCritical() << "TimeseriesDatabase::read(): Could not execute query: " << query.lastError().text();
        return false;
    }

    while (query.next()){
        try{
            const uint    uid   = query.value("uid").toUInt();
            const uint    rank  = query.value("rank").toUInt();
            const QString title = query.value("title").toString();

            Timeseries *ts = new Timeseries(uid,parent);
            ts->setRank(rank);
            ts->setTitle(title);
            list.push_back(ts);
        } catch (std::exception e) {
            qCritical() << "TimeSeriesDatabase::readTimeSeries(): could not parse a time series: " << e.what()
                        << " - Ignore and continue with next.";
        }
    }

    return true;
}

Timeseries *TimeseriesDatabase::insert(const QString &title, QObject *parent){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "TimeseriesDatabase::insert(): Could not open database: " << db.lastError().text();
        return nullptr;
    }

    // update ranks
    QSqlQuery queryRanks("UPDATE timeseries SET rank=rank+1",db);
    if (queryRanks.lastError().type()){
        qCritical() << "TimeseriesDatabase::insert(): Could not update ranks: " << queryRanks.lastError().text();
        return nullptr;
    }

    // insert time series
    QSqlQuery query("INSERT INTO timeseries (rank,title) VALUES(1,'"+title+"')",db);
    if (query.lastError().type()){
        qCritical() << "TimeseriesDatabase::insert(): Could not insert new time series: " << query.lastError().text();
        return nullptr;
    }

    Timeseries *ts = new Timeseries(query.lastInsertId().toInt(),parent);
    ts->setTitle(title);
    ts->setRank(1);
    return ts;
}

bool TimeseriesDatabase::update(const Timeseries *ts){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "TimeseriesDatabase::update(): Could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query("UPDATE timeseries SET "
                    "rank="+QString::number(ts->rank())+","
                    "title='"+ts->title()+"' "
                    "WHERE uid="+QString::number(ts->uid())
                    ,db);
    if (query.lastError().type()){
        qCritical() << "TimeseriesDatabase::update(): Could not update time series: " << query.lastError().text();
        return false;
    }

    return true;

}

bool TimeseriesDatabase::remove(uint uid){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << "TimeseriesDatabase::remove(): Could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query("DELETE FROM timeseries WHERE uid="+QString::number(uid),db);
    if (query.lastError().type()) {
        qCritical() << "TimeseriesDatabase::remove(): Could not delete time series: " << query.lastError().text();
        return false;
    }

    return true;
}
