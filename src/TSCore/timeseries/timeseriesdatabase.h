/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QStandardPaths>
#include <QDir>
#include <QDebug>
#include <QString>

#include "timeseries.h"

class TimeseriesDatabase
{
public:
    TimeseriesDatabase();

    // time series manipulation
    bool read(QList<Timeseries*> &list, QObject *parent);
    Timeseries *insert(const QString &title, QObject *parent = nullptr);
    bool update(const Timeseries *ts);
    bool remove(uint uid);

private:
    const QString m_db_name = "timeseries";
};
