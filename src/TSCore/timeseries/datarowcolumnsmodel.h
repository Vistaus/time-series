/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QAbstractListModel>

#include "datacolumn.h"

class DataRowColumnsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)

public:
    struct Node {
        Node (DataColumn *c = nullptr, const qreal v = 0.)
            : column(c), value(v) {}

        DataColumn *column;
        qreal       value;
    };

    enum Roles {
        COLUMNROLE = Qt::UserRole,
        VALUEROLE
    };

    explicit DataRowColumnsModel(const QVector<DataColumn*> &cols = {}, QObject *parent = nullptr);

    // QML properties - getter
    int count() const {return m_list.size();}

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    // QML interface
    Q_INVOKABLE void clear();
    Q_INVOKABLE void insertValue(DataColumn *col, const qreal value);

    // C++ interface
    const QVector<Node> &list() const {return m_list;}
    bool  contains(DataColumn *col) const {return col ? contains(col->uid()) : false;}
    bool  contains(const uint uid) const;
    qreal valueAt(const uint uid) const;

    void setList(const QVector<Node> &list);
    void setValue(const uint colId, const qreal value);
    void remove(DataColumn *col);

signals:
    void countChanged();

private:
    QVector<Node> m_list;
};
