/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QAbstractListModel>

#include "timeseries.h"

class TimeseriesModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)

public:
    enum Roles{
        TIMESERIESROLE = Qt::UserRole
    };

    explicit TimeseriesModel(QObject *parent = nullptr) : QAbstractListModel(parent){}

    // QML properties - getter
    int count() const {return m_list.size();}

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    // QML interface
    Q_INVOKABLE int indexOf(const uint uid) const;
    Q_INVOKABLE Timeseries *at(const int idx) const;
    Q_INVOKABLE QVariantList titles() const;

    // C++ interface
    Timeseries *find(const uint uid) const;

    void insertTimeseries(Timeseries *ts);
    void removeTimeseries(const uint uid);
    void swapTimeseries(const int idx1, const int idx2);

signals:
    void countChanged();

private:
    QVector<Timeseries*> m_list;

};
