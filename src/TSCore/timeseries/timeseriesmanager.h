/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>
#include <QDebug>
#include <QDateTime>

#include "timeseriesdatabase.h"
#include "timeseriesmodel.h"

#include "datacolumnmodel.h"

class TimeseriesManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(TimeseriesModel *model       READ model       CONSTANT)
    Q_PROPERTY(DataColumnModel *newColumns  READ newColumns  CONSTANT)
    Q_PROPERTY(DataColumnModel *editColumns READ editColumns CONSTANT)
    Q_PROPERTY(Timeseries      *selected   READ selected   WRITE setSelected NOTIFY selectedChanged)

public:
    explicit TimeseriesManager(QObject *parent = nullptr);

    // QML properties - getter
    TimeseriesModel *model()       const {return m_model;}
    DataColumnModel *newColumns()  const {return m_newColumns;}
    DataColumnModel *editColumns() const {return m_editColumns;}
    Timeseries      *selected()    const {return m_selected;}

    // QML interface
    Q_INVOKABLE void insert(const QString &title);
    Q_INVOKABLE void update(const uint uid, const QString &title);
    Q_INVOKABLE void remove(const uint uid);
    Q_INVOKABLE void swap(const int idx1, const int idx2);

    Q_INVOKABLE void prepareTimeseriesForEdit(Timeseries *ts);

public slots:
    void setSelected(Timeseries *selected);

signals:
    void selectedChanged();

private:
    TimeseriesDatabase m_db;

    // QML properties - members
    TimeseriesModel *m_model       = nullptr;
    DataColumnModel *m_newColumns  = nullptr;
    DataColumnModel *m_editColumns = nullptr;
    Timeseries      *m_selected    = nullptr;
};
