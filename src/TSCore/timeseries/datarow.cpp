/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "datarow.h"

DataRow::DataRow(QObject *parent)
    : QObject(parent), m_columns(new DataRowColumnsModel({},this))
{
    //qDebug() << "DataRow: created.";
}

DataRow::DataRow(const QVector<DataColumn *> &cols, QObject *parent)
    : QObject(parent), m_columns(new DataRowColumnsModel(cols,this))
{
    //qDebug() << "DataRow: created.";
}

DataRow::~DataRow(){
    //qDebug() << "DataRow: deleted.";
    delete m_columns;
}

bool DataRow::contains(const uint colId) const{
    return m_columns->contains(colId);
}

qreal DataRow::valueAt(const uint colId) const{
    return m_columns->valueAt(colId);
}

void DataRow::insertValue(DataColumn *col, const qreal value){
    m_columns->insertValue(col,value);
}

void DataRow::removeColumn(DataColumn *col){
    m_columns->remove(col);
}

void DataRow::setColumns(const QVector<DataRowColumnsModel::Node> &cols){
    m_columns->setList(cols);
}

void DataRow::setDate(const QDate &date){
    if (date == m_date)
        return;

    m_date = date;
    emit dateChanged();
}

void DataRow::setUid(const uint uid){
    if (uid == m_uid)
        return;

    m_uid = uid;
    emit uidChanged();

}
