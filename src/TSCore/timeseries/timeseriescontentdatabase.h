/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QStandardPaths>
#include <QDir>
#include <QDebug>
#include <QString>
#include <QList>
#include <QMap>
#include <QDate>

#include "datacolumn.h"
#include "datarow.h"

class TimeSeriesContentDatabase
{
public:
    TimeSeriesContentDatabase(const uint tsUid);
    ~TimeSeriesContentDatabase();

    typedef QMap<uint,QPair<QDate,QMap<uint,qreal>>> RowDataMap;

    QList<DataColumn*> readColumns(QObject *parent = nullptr);
    bool insertColumn(DataColumn *col);
    bool updateColumn(DataColumn *col);
    bool removeColumn(const uint uid);

    // returns a map {rowId : {colId : value } }
    RowDataMap readDataPoints();
    bool insertDataRow(DataRow *row);
    bool removeDataRow(DataRow *row);
    bool updateValue(DataColumn *col, DataRow *row, const qreal value);

private:
    const QString m_db_name;

    const QString m_dateFormat = "yyyy/MM/dd";

    QString prompt(const QString &context) const{
        return "TimeseriesDatabase::"+context+"(): ";
    }

public:
    const QString filename;
};
