/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>
#include <QDebug>
#include <QDate>

#include "datarowcolumnsmodel.h"

class DataRow : public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint                 uid     READ uid     NOTIFY uidChanged)
    Q_PROPERTY(QDate                date    READ date    WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(DataRowColumnsModel *columns READ columns CONSTANT)

public:
    explicit DataRow(QObject *parent = nullptr);
    explicit DataRow(const QVector<DataColumn*> &cols, QObject *parent = nullptr);
    ~DataRow();

    // QML properties - getter
    uint                 uid()     const {return m_uid;}
    QDate                date()    const {return m_date;}
    DataRowColumnsModel *columns() const {return m_columns;}

    // QML interface
    Q_INVOKABLE bool  contains(DataColumn *col) const {return col ? contains(col->uid()) : false;}
    Q_INVOKABLE bool  contains(const uint colId) const;
    Q_INVOKABLE qreal valueAt(DataColumn *col) const {return col ? valueAt(col->uid()) : 0.;}
    Q_INVOKABLE qreal valueAt(const uint colId) const;

    Q_INVOKABLE void insertValue(DataColumn *col, const qreal value);

    // C++ interrace
    void removeColumn(DataColumn *col);
    void setColumns(const QVector<DataRowColumnsModel::Node> &cols);

public slots:
    // QML properties - setter
    void setDate(const QDate &date);

    // C++ setter
    void setUid(const uint uid);

signals:
    void uidChanged();
    void dateChanged();

private:
    uint                 m_uid = 0;
    QDate                m_date;
    DataRowColumnsModel *m_columns = nullptr;

};
