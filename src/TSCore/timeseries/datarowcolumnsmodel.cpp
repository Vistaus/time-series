/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "datarowcolumnsmodel.h"

DataRowColumnsModel::DataRowColumnsModel(const QVector<DataColumn *> &cols, QObject *parent)
    : QAbstractListModel(parent) {
    // insert columns by idx
    for (DataColumn *col : cols){
        int insertIdx = 0;
        while (insertIdx < m_list.size() && m_list.at(insertIdx).column->idx() < col->idx())
            insertIdx++;
        m_list.insert(insertIdx,{col,0.});
    }
}

int DataRowColumnsModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant DataRowColumnsModel::data(const QModelIndex &index, int role) const{
    switch (role) {
    case COLUMNROLE:
        return QVariant::fromValue<DataColumn*>(m_list.at(index.row()).column);
    case VALUEROLE:
        return QVariant(m_list.at(index.row()).value);
    }
    return QVariant();
}

QHash<int, QByteArray> DataRowColumnsModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[COLUMNROLE] = "modelColumn";
    names[VALUEROLE]  = "modelValue";
    return names;
}

void DataRowColumnsModel::clear(){
    if (m_list.isEmpty())
        return;

    beginResetModel();
    m_list.clear();
    endResetModel();
    emit countChanged();
}

void DataRowColumnsModel::insertValue(DataColumn *col, const qreal value){
    // update value if col is already in list
    int idx = 0;
    for (Node &node : m_list){
        if (node.column->uid() == col->uid()){
            node.value = value;
            emit dataChanged(index(idx),index(idx),{VALUEROLE});
            return;
        }
        idx++;
    }

    // need to insert column
    int insertIdx = 0;
    while (insertIdx < m_list.size() && m_list.at(insertIdx).column->idx() < col->idx())
        insertIdx++;
    beginInsertRows(QModelIndex(),insertIdx,insertIdx);
    m_list.insert(insertIdx,{col,value});
    endInsertRows();
    emit countChanged();
}

bool DataRowColumnsModel::contains(const uint uid) const{
    for (const Node &node : m_list)
        if (node.column->uid() == uid)
            return true;
    return false;
}

qreal DataRowColumnsModel::valueAt(const uint uid) const{
    for (const Node &node : m_list)
        if (node.column->uid() == uid)
            return node.value;
    return 0.;
}

void DataRowColumnsModel::setList(const QVector<Node> &list){
    beginResetModel();
    m_list = list;
    endResetModel();
    emit countChanged();
}

void DataRowColumnsModel::setValue(const uint colId, const qreal value){
    int idx = 0;
    for (Node &node : m_list){
        if (node.column->uid() == colId){
            node.value = value;
            emit dataChanged(index(idx),index(idx),{VALUEROLE});
        }
        idx++;
    }
}

void DataRowColumnsModel::remove(DataColumn *col){
    int idx = 0;
    for (Node &node : m_list){
        if (node.column->uid() == col->uid()){
            beginRemoveRows(QModelIndex(),idx,idx);
            m_list.remove(idx);
            endRemoveRows();
            emit countChanged();
        }
        idx++;
    }
}
