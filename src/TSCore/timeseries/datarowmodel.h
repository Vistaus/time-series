/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QAbstractListModel>
#include <QDebug>
#include <QVector>

#include "datarow.h"


class DataRowModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)

public:
    enum Roles{
        ROWROLE = Qt::UserRole,
        VALUECHANGESROLE
    };

    explicit DataRowModel(QObject *parent = nullptr) : QAbstractListModel(parent) {}

    // QML properties
    int count() const {return m_list.size();}

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    // QML interface
    Q_INVOKABLE qreal getLastValue(const uint colId) const;

    // C++ interface
    bool contains(const uint uid) const;
    qreal valueAt(const int idx, DataColumn *col) const {return col ? valueAt(idx,col->uid()) : 0.;}
    qreal valueAt(const int idx, const uint colId) const;
    QDate dateAt(const int idx) const;
    DataRow *at(const int idx) const;
    QList<qreal> valuesInTimeRange(DataColumn *col, const QDate &firstDate, const QDate &lastDate);

    int  insert(DataRow *row);
    int  remove(const uint uid);
    int  updateValue(DataColumn *col, DataRow *row, const qreal value);
    void valuesChanged(const int idx, const int lastIdx = 0);

    typedef QVector<DataRow*> RowList;
    RowList::const_iterator begin() const {return m_list.begin();}
    RowList::const_iterator end()   const {return m_list.end();}

signals:
    void countChanged();

private:
    RowList m_list;
};
