/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "datacolumnmodel.h"

DataColumnModel::~DataColumnModel(){
    clear();
}

int DataColumnModel::rowCount(const QModelIndex &parent) const{
    return m_list.size();
}

QVariant DataColumnModel::data(const QModelIndex &index, int role) const{
    switch (role) {
    case COLUMNROLE:
        return QVariant::fromValue<DataColumn*>(m_list.at(index.row()));
    }
    return QVariant();
}

QHash<int, QByteArray> DataColumnModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[COLUMNROLE] = "dataColumn";
    return names;
}

int DataColumnModel::idxAt(int idx) const{
    return (idx < 0 || idx >= m_list.size()) ? -1 : m_list.at(idx)->idx();
}

QString DataColumnModel::titleAt(int idx) const{
    return (idx < 0 || idx >= m_list.size()) ? QString() : m_list.at(idx)->title();
}

QVariantList DataColumnModel::getDependentIndices(int idx) const{
    if (idx < 0 || idx >= m_list.size())
        return QVariantList();
    QVector<uint> deps = getDependentIndices(m_list.at(idx));

    // remove dublicates and own idx
    for (int i=0; i<deps.size(); i++)
        if (deps.at(i) == idx || deps.lastIndexOf(deps.at(i)) > i)
            deps.removeAt(i--);


    // sort
    QList<uint> sortedDeps;
    for (int dep : deps){
        int insertIdx = 0;
        while (insertIdx < sortedDeps.size() || sortedDeps.at(insertIdx) < dep)
            insertIdx++;
        sortedDeps.insert(insertIdx,dep);
    }

    // translate to QVariantList
    QVariantList varDeps;
    for (int dep : sortedDeps)
        varDeps.append(QVariant(dep));
    return varDeps;
}

void DataColumnModel::clear(){
    if (m_list.isEmpty())
        return;

    beginResetModel();
    QVector<DataColumn*> cols = m_list;
    m_list.clear();
    endResetModel();
    emit countChanged();

    // all columns that are parented to this model have to be deleted
    for (DataColumn *col : cols)
        if (col->parent() == this)
            delete col;
}

void DataColumnModel::insertLocalColumn(DataColumn *col){
    col->setParent(this);

    // set index to lowest available idx
    int idx = 0;
    for (DataColumn *col : m_list)
        if (col->idx() >= idx)
            idx = col->idx()+1;
    col->setIdx(idx);

    if (col->type() != DataColumn::RAWDATA){
        col->setAllowNegative(true);
        col->setDigits(1);
    }

    insertColumn(col);
}

void DataColumnModel::removeColumnAt(const int idx){
    if (idx < 0 || idx >= m_list.size())
        return;

    // get dependencies
    QVector<uint> deleteList = getDependentIndices(m_list.at(idx));
    for (int i=0; i<deleteList.size(); i++)
        if (deleteList.lastIndexOf(deleteList.at(i)) > i)
            deleteList.removeAt(i--);

    for (int i=0; i<deleteList.size(); i++){
        const int curIdx = deleteList.at(i);
        DataColumn *curCol = m_list.at(curIdx);
        beginRemoveRows(QModelIndex(),curIdx,curIdx);
        m_list.remove(curIdx);
        endRemoveRows();
        emit countChanged();

        // update references in columns
        for (DataColumn *col : m_list){
            if (col->idx() > curIdx)
                col->setIdx(col->idx()-1);
            if (col->baseColumn1() > curIdx)
                col->setBaseColumn1(col->baseColumn1()-1);
            if (col->baseColumn2() > curIdx)
                col->setBaseColumn2(col->baseColumn2()-1);
        }

        // update indices in deleteList
        for (int k=i+1; k<deleteList.size(); k++)
            if (deleteList.at(k) > curIdx)
                deleteList[k] -= 1;

        // delete curCol if needed
        if (curCol->parent() == this)
            delete curCol;
    }
}

void DataColumnModel::swap(const int idx1, const int idx2){
    if (idx1 < 0 || idx1 >= m_list.size() || idx2 < 0 || idx2 >= m_list.size())
        return;

    // update all references
    for (DataColumn *col : m_list){
        if (col->baseColumn1() == idx1)
            col->setBaseColumn1(idx2);
        else if (col->baseColumn1() == idx2)
            col->setBaseColumn1(idx1);
        if (col->baseColumn2() == idx1)
            col->setBaseColumn2(idx2);
        else if (col->baseColumn2() == idx2)
            col->setBaseColumn2(idx1);
    }

    // update idx properties
    m_list[idx1]->setIdx(idx2);
    m_list[idx2]->setIdx(idx1);

    // swap entries
    beginMoveRows(QModelIndex(),idx1,idx1,QModelIndex(),idx2);
    m_list.move(idx1,idx2);
    endMoveRows();
}

DataColumn *DataColumnModel::getByIdx(const int idx) const{
    for (DataColumn *col : m_list)
        if (col->idx() == idx)
            return col;
    return nullptr;
}

DataColumn *DataColumnModel::get(const uint uid) const{
    for (DataColumn *col : m_list)
        if (col->uid() == uid)
            return col;
    return nullptr;
}

bool DataColumnModel::contains(const uint uid) const{
    for (DataColumn *col : m_list)
        if (col->uid() == uid)
            return true;
    return false;
}

void DataColumnModel::insertColumn(DataColumn *col){
    int insertIdx = 0;
    while (insertIdx < m_list.size() && m_list.at(insertIdx)->idx() < col->idx())
        insertIdx++;

    beginInsertRows(QModelIndex(),insertIdx,insertIdx);
    m_list.insert(insertIdx,col);
    endInsertRows();
    emit countChanged();
}

void DataColumnModel::removeColumn(const uint uid){
    int idx = 0;
    for (DataColumn *col : m_list){
        if (col->uid() == uid){
            removeColumnAt(idx);
            return;
        }
        idx++;
    }
}

void DataColumnModel::resort(){
    const int L = m_list.size();
    for (int i=0; i<L; i++){
        int lowestIdx    = m_list.at(i)->idx();
        int lowestIdxPos = i;
        for (int j=i+1; j<L; j++){
            if (m_list.at(j)->idx() < lowestIdx){
                lowestIdx = m_list.at(j)->idx();
                lowestIdxPos = j;
            }
        }
        if (lowestIdxPos != i){
            beginMoveRows(QModelIndex(),lowestIdxPos,lowestIdxPos,QModelIndex(),i);
            m_list.move(lowestIdxPos,i);
            endMoveRows();
        }
    }
}

QVector<uint> DataColumnModel::getDependentIndices(const DataColumn *col) const{
    QVector<uint> deps = {col->idx()};
    for(DataColumn *curCol : m_list){
        // check if curCol's first base column is col
        switch (curCol->type()) {
        case DataColumn::AGGREGATION:
        case DataColumn::GRADIENT:
        case DataColumn::AVERAGE:
        case DataColumn::SUM:
        case DataColumn::DIFFERENCE:
        case DataColumn::PRODUCT:
        case DataColumn::QUOTIENT:
            if (curCol->baseColumn1() == col->idx())
                deps.append(getDependentIndices(curCol));
        }
        // check if curCol's second base column is col
        switch (curCol->type()) {
        case DataColumn::SUM:
        case DataColumn::DIFFERENCE:
        case DataColumn::PRODUCT:
        case DataColumn::QUOTIENT:
            if (curCol->baseColumn2() == col->idx())
                deps.append(getDependentIndices(curCol));
        }
    }
    return deps;

}
