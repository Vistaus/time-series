/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timeseriesmanager.h"

TimeseriesManager::TimeseriesManager(QObject *parent)
    : QObject(parent)
{
    const qint64 begin = QDateTime::currentMSecsSinceEpoch();

    m_model       = new TimeseriesModel(this);
    m_newColumns  = new DataColumnModel(this);
    m_editColumns = new DataColumnModel(this);

    QList<Timeseries*> tslist;
    m_db.read(tslist,this);
    for (Timeseries *ts : tslist)
        m_model->insertTimeseries(ts);

    const qint64 duration = QDateTime::currentMSecsSinceEpoch() - begin;
    qInfo() << "TimeseriesManager: initialisation took" << duration << "ms.";
}


void TimeseriesManager::insert(const QString &title){
    Timeseries *ts = m_db.insert(title,this);
    if (ts){
        for (DataColumn *col : m_newColumns->list())
            ts->insertColumn(col);
        m_model->insertTimeseries(ts);
    } else {
        qWarning() << "TimeseriesManager::insert() - could not insert time series!";
    }
}

void TimeseriesManager::update(const uint uid, const QString &title){
    Timeseries *ts = m_model->find(uid);
    if (!ts)
        return;

    const QString oldTitle = ts->title();
    ts->setTitle(title);

    if (m_db.update(ts)){
        // remove deleted columns
        for (int i=ts->columns()->count()-1; i>=0; i--){
            DataColumn *col = ts->columns()->list().at(i);
            if (!m_editColumns->contains(col))
                ts->removeColumn(col->uid());
        }

        for (DataColumn *col : editColumns()->list()){
            if (col->uid() == 0){
                // insert new columns
                ts->insertColumn(col);
            } else {
                // update existing columns.
                // Only the following properties will be updated:
                //     idx, title, unit, usePreviousAsDefault, defaultValue,
                //     allowNegative, digits, precision,
                //     baseColumn1, baseColumn2, referenceTime
                ts->updateColumn(col);
            }
        }

        // make timeseries recompute all derived values
        ts->refreshValues();

        // clear editColumns model
        m_editColumns->clear();
    } else {
        // rollback changes
        ts->setTitle(oldTitle);
    }
}

void TimeseriesManager::remove(const uint uid){
    Timeseries *ts = m_model->find(uid);
    if (!ts)
        return;

    ts->deleteTimeseries();

    if (m_db.remove(uid)){
        m_model->removeTimeseries(uid);
        delete ts;
    }
}

void TimeseriesManager::swap(const int idx1, const int idx2){
    Timeseries *ts1 = m_model->at(idx1);
    Timeseries *ts2 = m_model->at(idx2);
    if (!ts1 || !ts2)
        return;

    // swap ranks
    const int rank = ts1->rank();
    ts1->setRank(ts2->rank());
    ts2->setRank(rank);

    if (m_db.update(ts1) && m_db.update(ts2)){
        m_model->swapTimeseries(idx1,idx2);
    } else {
        // swap back ranks
        const int rank = ts1->rank();
        ts1->setRank(ts2->rank());
        ts2->setRank(rank);
    }
}

void TimeseriesManager::prepareTimeseriesForEdit(Timeseries *ts){
    // copy columns to editColumns model
    m_editColumns->clear();
    for (DataColumn *col : ts->columns()->list())
        m_editColumns->insertColumn(new DataColumn(col,m_editColumns));
}

void TimeseriesManager::setSelected(Timeseries *selected){
    if (m_selected == selected)
        return;

    if (m_selected)
        m_selected->deselect();
    if (selected)
        selected->select();

    m_selected = selected;
    emit selectedChanged();
}
