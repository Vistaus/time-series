/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>
#include <QDebug>
#include <QString>

class DataColumn : public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint    uid   READ uid   WRITE setUid   NOTIFY uidChanged)
    Q_PROPERTY(uint    idx   READ idx   WRITE setIdx   NOTIFY idxChanged)
    Q_PROPERTY(Type    type  READ type  WRITE setType  NOTIFY typeChanged)
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QString unit  READ unit  WRITE setUnit  NOTIFY unitChanged)

    // default values
    Q_PROPERTY(bool  usePreviousAsDefault READ usePreviousAsDefault WRITE setUsePreviousAsDefault NOTIFY usePreviousAsDefaultChanged)
    Q_PROPERTY(qreal defaultValue         READ defaultValue         WRITE setDefaultValue         NOTIFY defaultValueChanged)

    // visualisation
    Q_PROPERTY(bool allowNegative READ allowNegative WRITE setAllowNegative NOTIFY allowNegativeChanged)
    Q_PROPERTY(uint digits        READ digits        WRITE setDigits        NOTIFY digitsChanged)
    Q_PROPERTY(uint precision     READ precision     WRITE setPrecision     NOTIFY precisionChanged)

    // references
    Q_PROPERTY(int baseColumn1   READ baseColumn1   WRITE setBaseColumn1   NOTIFY baseColumn1Changed)
    Q_PROPERTY(int baseColumn2   READ baseColumn2   WRITE setBaseColumn2   NOTIFY baseColumn2Changed)
    Q_PROPERTY(int referenceTime READ referenceTime WRITE setReferenceTime NOTIFY referenceTimeChanged)

    // UI properties
    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)

public:
    enum Type {
        RAWDATA     = 0,
        AGGREGATION = 1,
        GRADIENT    = 2,
        AVERAGE     = 3,
        SUM         = 4,
        DIFFERENCE  = 5,
        PRODUCT     = 6,
        QUOTIENT    = 7,
        NUMBER_OF_TYPES
    };
    Q_ENUMS(Type)

    enum GradientMode{
        GRADIENT_INVALID  = 500,
        GRADIENT_ABSOLUTE = 501,
        GRADIENT_PERDAY   = 502
    };

    explicit DataColumn(QObject *parent = nullptr);
    DataColumn(DataColumn *col, QObject *parent = nullptr);
    ~DataColumn();

    // QML properties - getter
    uint    uid()   const {return m_uid;}
    uint    idx()   const {return m_idx;}
    Type    type()  const {return m_type;}
    QString title() const {return m_title;}
    QString unit()  const {return m_unit;}
    bool  usePreviousAsDefault() const {return m_usePreviousAsDefault;}
    qreal defaultValue()         const {return m_defaultValue;}
    bool allowNegative() const {return m_allowNegative;}
    uint digits()        const {return m_digits;}
    uint precision()     const {return m_precision;}
    int  baseColumn1()   const {return m_baseColumn1;}
    int  baseColumn2()   const {return m_baseColumn2;}
    int  referenceTime() const {return m_referenceTime;}
    int  width()         const {return m_width;}

    // QML interface
    Q_INVOKABLE QString formatValue(const qreal value) const;

    // C++ interface
    GradientMode gradientMode() const;

public slots:
    // QML interface - setter
    void setUid  (const uint uid);
    void setIdx  (const uint idx);
    void setType (const Type type);
    void setTitle(const QString &title);
    void setUnit (const QString &unit);
    void setUsePreviousAsDefault(const bool  usePreviousAsDefault);
    void setDefaultValue        (const qreal defaultValue);
    void setAllowNegative(const bool allowNegative);
    void setDigits       (const uint digits);
    void setPrecision    (const uint precision);
    void setBaseColumn1  (const int  baseColumn1);
    void setBaseColumn2  (const int  baseColumn2);
    void setReferenceTime(const int  referenceTime);
    void setWidth        (const int  width);

signals:
    void uidChanged();
    void idxChanged();
    void typeChanged();
    void titleChanged();
    void unitChanged();
    void usePreviousAsDefaultChanged();
    void defaultValueChanged();
    void allowNegativeChanged();
    void digitsChanged();
    void precisionChanged();
    void baseColumn1Changed();
    void baseColumn2Changed();
    void referenceTimeChanged();
    void widthChanged();

private:
    uint    m_uid  = 0;
    uint    m_idx  = 0;
    Type    m_type = RAWDATA;
    QString m_title;
    QString m_unit;
    bool    m_usePreviousAsDefault = false;
    qreal   m_defaultValue         = 0.;
    bool    m_allowNegative        = false;
    uint    m_digits               = 1;
    uint    m_precision            = 0;
    int     m_baseColumn1          = -1;
    int     m_baseColumn2          = -1;
    int     m_referenceTime        = 0;
    int     m_width = 0;
};
