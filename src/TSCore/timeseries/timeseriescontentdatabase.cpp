/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timeseriescontentdatabase.h"

TimeSeriesContentDatabase::TimeSeriesContentDatabase(const uint tsUid)
    : m_db_name("ts"+QString::number(tsUid))
    , filename(QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "/" + m_db_name + ".sqlite")
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",m_db_name);
    db.setDatabaseName(filename);
    if (!db.open())
        qCritical() << prompt("TimeSeriesContentDatabase") << "could not open database: " << db.lastError().text();

    // init columns table
    QSqlQuery query("CREATE TABLE IF NOT EXISTS columns ("
                    "uid INTEGER PRIMARY KEY,"
                    "idx INTEGER DEFAULT 0,"
                    "type INTEGER DEFAULT 0,"
                    "title TEXT DEFAULT '',"
                    "unit TEXT DEFAULT '',"
                    "usePreviousAsDefault INTEGER DEFAULT 0,"
                    "defaultValue REAL DEFAULT 0,"
                    "allowNegative INTEGER DEFAULT 0,"
                    "digits INTEGER DEFAULT 1,"
                    "precision INTEGER DEFAULT 0,"
                    "baseCol1 INTEGER DEFAULT 0,"
                    "baseCol2 INTEGER DEFAULT 0,"
                    "referenceTime INTEGER DEFAULT 0"
                    ")"
                    ,db);
    if (query.lastError().type())
        qCritical() << prompt("TimeSeriesContentDatabase") << "could not create table 'columns': " << query.lastError().type();

    // init rows table
    QSqlQuery queryRows("CREATE TABLE IF NOT EXISTS rows ("
                         "rowId INTEGER PRIMARY KEY,"
                         "date TEXT DEFAULT '')"
                        ,db);
    if (queryRows.lastError().type())
        qCritical() << prompt("TimeSeriesContentDatabase") << "could not create table 'rows': " << queryRows.lastError().type();

    // init data points table
    QSqlQuery queryDataPoints("CREATE TABLE IF NOT EXISTS datapoints ("
                              "colId INTEGER DEFAULT 0,"
                              "rowId INTEGER DEFAULT 0,"
                              "value REAL DEFAULT 0)"
                             ,db);
    if (queryDataPoints.lastError().type())
        qCritical() << prompt("TimeSeriesContentDatabase") << "could not create table 'datapoints': " << queryDataPoints.lastError().type();
}

TimeSeriesContentDatabase::~TimeSeriesContentDatabase(){
    QSqlDatabase::removeDatabase(m_db_name);
}

QList<DataColumn*> TimeSeriesContentDatabase::readColumns(QObject *parent){
    QList<DataColumn*> list;

    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << prompt("readColumns") << "could not open database: " << db.lastError().text();
        return list;
    }

    QSqlQuery query("SELECT * FROM columns",db);
    if (query.lastError().type()){
        qCritical() << prompt("readColumns") << "could not select columns: " << query.lastError().text();
        return list;
    }

    while (query.next()){
        DataColumn *col = new DataColumn(parent);
        try{
            col->setUid(query.value("uid").toUInt());
            col->setIdx(query.value("idx").toUInt());
            col->setType((DataColumn::Type) query.value("type").toInt());
            col->setTitle(query.value("title").toString());
            col->setUnit(query.value("unit").toString());
            col->setUsePreviousAsDefault(query.value("usePreviousAsDefault").toString() == "1");
            col->setDefaultValue(query.value("defaultValue").toReal());
            col->setAllowNegative(query.value("allowNegative").toString() == "1");
            col->setDigits(query.value("digits").toUInt());
            col->setPrecision(query.value("precision").toUInt());
            col->setBaseColumn1(query.value("baseCol1").toUInt());
            col->setBaseColumn2(query.value("baseCol2").toUInt());
            col->setReferenceTime(query.value("referenceTime").toInt());

            list.append(col);
        } catch (std::exception e) {
            qCritical() << prompt("readColumns") << "could not parse a column: " << e.what()
                        << " - Ignore and continue with next.";
        }
    }
    return list;
}

bool TimeSeriesContentDatabase::insertColumn(DataColumn *col){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << prompt("insertColumn") << "could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query("INSERT INTO columns ("
                    "idx,type,title,unit,usePreviousAsDefault,defaultValue,allowNegative,digits,precision,baseCol1,baseCol2,referenceTime"
                    ") VALUES ("
                    +QString::number(col->idx())+","
                    +QString::number(col->type())+","
                    "'"+col->title()+"',"
                    "'"+col->unit()+"',"
                    +(col->usePreviousAsDefault() ? "1" : "0")+","
                    +QString::number(col->defaultValue())+","
                    +(col->allowNegative() ? "1" : "0")+","
                    +QString::number(col->digits())+","
                    +QString::number(col->precision())+","
                    +QString::number(col->baseColumn1())+","
                    +QString::number(col->baseColumn2())+","
                    +QString::number(col->referenceTime())+
                    ")"
                    ,db);
    if (query.lastError().type()){
        qCritical() << prompt("insertColumn") << "could not insert column: " << query.lastError().text();
        return false;
    }

    col->setUid(query.lastInsertId().toUInt());
    return true;
}

bool TimeSeriesContentDatabase::updateColumn(DataColumn *col){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << prompt("updateColumn") << "could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query("UPDATE columns SET "
                    "idx="+QString::number(col->idx())+","
                    "title='"+col->title()+"',"
                    "unit='"+col->unit()+"',"
                    "usePreviousAsDefault="+(col->usePreviousAsDefault() ? "1" : "0")+","
                    "defaultValue="+QString::number(col->defaultValue())+","
                    "allowNegative="+(col->allowNegative() ? "1" : "0")+","
                    "digits="+QString::number(col->digits())+","
                    "precision="+QString::number(col->precision())+","
                    "baseCol1="+QString::number(col->baseColumn1())+","
                    "baseCol2="+QString::number(col->baseColumn2())+","
                    "referenceTime="+QString::number(col->referenceTime())+
                    " WHERE uid="+QString::number(col->uid())
                    ,db);
    if (query.lastError().type()){
        qCritical() << prompt("updateColumn") << "could not update column: " << query.lastError().text();
        return false;
    }

    return true;
}

bool TimeSeriesContentDatabase::removeColumn(const uint uid){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << prompt("removeColumn") << "could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery query("DELETE FROM columns WHERE uid="+QString::number(uid),db);
    if (query.lastError().type()){
        qCritical() << prompt("removeColumn") << "could not delete column: " << query.lastError().text();
        return false;
    }

    QSqlQuery queryData("DELETE FROM datapoints WHERE colId="+QString::number(uid),db);
    if (queryData.lastError().type()){
        qCritical() << prompt("removeColumn") << "could not delete datapoints: " << queryData.lastError().text();
        return false;
    }

    return true;
}

TimeSeriesContentDatabase::RowDataMap TimeSeriesContentDatabase::readDataPoints(){
    RowDataMap map;

    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << prompt("readDataPoints") << "could not open database: " << db.lastError().text();
        return map;
    }

    QSqlQuery queryRows("SELECT * FROM rows",db);
    if (queryRows.lastError().type()){
        qCritical() << prompt("readDataPoints") << "could not select rows: " << queryRows.lastError().text();
        return map;
    }

    while (queryRows.next()){
        try{
            const uint  rowId = queryRows.value("rowId").toUInt();
            const QDate date = QDate::fromString(queryRows.value("date").toString(),m_dateFormat);

            map[rowId] = {date,{}};
        } catch (std::exception e) {
            qWarning() << prompt("readDataPoints") << "could not parse a row: " << e.what()
                       << " - Ignore and continue with next.";
        }
    }

    QSqlQuery queryDatapoints("SELECT * FROM datapoints",db);
    if (queryDatapoints.lastError().type()){
        qCritical() << prompt("readDataPoints") << "could not select datapoints: " << queryDatapoints.lastError().text();
        return map;
    }

    while (queryDatapoints.next()){
        try{
            const uint  rowId = queryDatapoints.value("rowId").toUInt();
            const uint  colId = queryDatapoints.value("colId").toUInt();
            const qreal value = queryDatapoints.value("value").toReal();

            if (map.contains(rowId))
                map[rowId].second[colId] = value;
            else
                qWarning() << prompt("readDataPoints") << "Found a data point to non existing rowId =" << rowId;
        } catch (std::exception e) {
            qWarning() << prompt("readDataPoints") << "could not parse a datapoint: " << e.what()
                       << " - Ignore and continue with next.";
        }
    }

    return map;
}

bool TimeSeriesContentDatabase::insertDataRow(DataRow *row){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << prompt("insertDataRow") << "could not open database: " << db.lastError().text();
        return false;
    }

    // create row object
    QSqlQuery queryRow("INSERT INTO rows (date) VALUES ("
                       "'"+row->date().toString(m_dateFormat)+"')",db);
    if (queryRow.lastError().type()){
        qCritical() << prompt("insertDataRow") << "could not insert row object: " << queryRow.lastError().text();
        return false;
    }
    row->setUid(queryRow.lastInsertId().toUInt());

    // create data points
    for (const DataRowColumnsModel::Node &node : row->columns()->list()){
        QSqlQuery queryData("INSERT INTO datapoints (rowId,colId,value) VALUES ("
                            +QString::number(row->uid())+","
                            +QString::number(node.column->uid())+","
                            +QString::number(node.value)+")"
                            ,db);
        if (queryData.lastError().type()){
            qCritical() << prompt("insertDataRow") << "could not insert a data point: " << queryData.lastError().text();
            return false;
        }
    }
    return true;
}

bool TimeSeriesContentDatabase::removeDataRow(DataRow *row){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << prompt("removeDataRow") << "could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery queryRow("DELETE FROM rows WHERE rowId="+QString::number(row->uid()),db);
    if (queryRow.lastError().type()){
        qCritical() << prompt("removeDataRow") << "could not delete row object: " << queryRow.lastError().text();
        return false;
    }

    QSqlQuery queryData("DELETE FROM datapoints WHERE rowId="+QString::number(row->uid()),db);
    if (queryData.lastError().type()){
        qCritical() << prompt("removeDataRow") << "could not delete data points: " << queryData.lastError().text();
        return false;
    }

    return true;
}

bool TimeSeriesContentDatabase::updateValue(DataColumn *col, DataRow *row, const qreal value){
    QSqlDatabase db = QSqlDatabase::database(m_db_name);
    if (!db.open()){
        qCritical() << prompt("updateValue") << "Could not open database: " << db.lastError().text();
        return false;
    }

    QSqlQuery queryDelete("DELETE FROM datapoints WHERE "
                          "rowId="+QString::number(row->uid()) + " AND "
                          "colId="+QString::number(col->uid())
                          ,db);
    if (queryDelete.lastError().type()){
        qCritical() << prompt("updateValue") << "could not delete old datapoint: " << queryDelete.lastError().text();
        return false;
    }

    QSqlQuery queryInsert("INSERT INTO datapoints (rowId,colId,value) VALUES ("
                          +QString::number(row->uid())+","
                          +QString::number(col->uid())+","
                          +QString::number(value)+")"
                          ,db);
    if (queryInsert.lastError().type()){
        qCritical() << prompt("updateValue") << "could not insert new datapoint: " << queryInsert.lastError().text();
        return false;
    }
    return true;
}
