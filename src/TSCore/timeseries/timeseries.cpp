/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timeseries.h"

Timeseries::Timeseries(const uint uid, QObject *parent)
    : QObject(parent), m_uid(uid)
    , m_columns(new DataColumnModel(this))
    , m_rows(new DataRowModel(this))
    , m_db(uid)
{
    // init columns
    for (DataColumn *col : m_db.readColumns(this)){
        m_columns->insertColumn(col);
        switch (col->type()) {
        case DataColumn::GRADIENT:
        case DataColumn::AGGREGATION:
        case DataColumn::AVERAGE:
            m_containsComplexColumns = true;
            break;
        }
    }

    // init data rows
    TimeSeriesContentDatabase::RowDataMap dataPoints = m_db.readDataPoints();
    for (auto it = dataPoints.begin(); it != dataPoints.end(); it++){
        // create data row by row
        DataRow *row = new DataRow(this);
        row->setUid(it.key());
        row->setDate(it.value().first);
        const auto &colMap = it.value().second;
        for (auto colIt = colMap.begin(); colIt != colMap.end(); colIt++){
            DataColumn *col = m_columns->get(colIt.key());
            if (col) {
                if (col->type() == DataColumn::RAWDATA)
                    row->insertValue(col,colIt.value());
                else
                    qWarning() << "Timeseries: found data point for row" << it.key()
                               << "with non-rawdata column" << colIt.key() << ". Ignore this point.";
            } else {
                qWarning() << "Timeseries: found data point for row" << it.key()
                           << "with non-existing column" << colIt.key() << ". Ignore this point.";
            }
        }
        doInsertRow(row);
    }
}

Timeseries::~Timeseries(){
    delete m_rows;
    delete m_columns;
}

void Timeseries::deleteTimeseries(){
    // delete database
    QDir().remove(m_db.filename);
}

QDate Timeseries::getMonthAt(const int row) const{
    if (m_monthlyBeforeCounts.isEmpty())
        return QDate::currentDate();

    QDate lastMonthWithCounts = m_monthlyBeforeCounts.firstKey();
    int lastCount = 0;
    for (auto it = m_monthlyBeforeCounts.begin(); it != m_monthlyBeforeCounts.end(); it++){
        // if beforeCounter of current month is > row index, then the lastMonthWithCounts is the month at this row
        if (it.value() > row)
            return lastMonthWithCounts;
        // if this month has any count at all, save this as lastMonthWithCounts
        if (it.value() > lastCount)
            lastMonthWithCounts = it.key();
        lastCount = it.value();
    }
    return lastMonthWithCounts;
}

int Timeseries::getBeforeCount(const QDate &month) const{
    const QDate monthEff = QDate(month.year(),month.month(),2);
    if (m_monthlyBeforeCounts.isEmpty())
        return 0;
    if (monthEff < m_monthlyBeforeCounts.firstKey())
        return 0;
    if (monthEff >= m_monthlyBeforeCounts.lastKey())
        return m_monthlyBeforeCounts.last();
    return m_monthlyBeforeCounts[monthEff];
}

void Timeseries::select(){
    m_isSelected = true;
    qInfo() << prompt() << "selected.";
}

void Timeseries::deselect(){
    m_isSelected = false;
    qInfo() << prompt() << "deselected.";
}

void Timeseries::insertRow(DataRow *row){
    DataRow *fullRow = new DataRow(this);
    fullRow->setDate(row->date());

    // get all raw data rows
    for (DataColumn *col : m_columns->list()){
        if (col->type() == DataColumn::RAWDATA){
            const uint colId = col->uid();
            qreal value = 0.;
            if (row->contains(colId)){
                // take value from row
                value = row->valueAt(colId);
            } else {
                // determine default value
                if (col->usePreviousAsDefault()){
                    // get last value
                    value = m_rows->getLastValue(colId);
                } else {
                    // take default value of column
                    value = col->defaultValue();
                }
            }
            fullRow->insertValue(col,value);
        }
    }

    // write row to database
    if (m_db.insertDataRow(fullRow))
        doInsertRow(fullRow);
}

void Timeseries::removeRow(DataRow *row){
    if (m_rows->contains(row->uid())){
        if (m_db.removeDataRow(row)){
            const QDate month = QDate(row->date().year(),row->date().month(),2);
            const int removeIdx = m_rows->remove(row->uid());
            if (m_containsComplexColumns){
                const int rowCount = m_rows->count();
                for (int curIdx = removeIdx; curIdx < rowCount; curIdx++)
                    doRecomputeRow(curIdx);
            }
            // update counter map
            for (auto it = m_monthlyBeforeCounts.begin(); it != m_monthlyBeforeCounts.end(); it++)
                if (it.key() > month)
                    it.value() -= 1;
        }
    }
}

void Timeseries::updateValue(DataColumn *col, DataRow *row, const qreal value){
    if (!m_columns->contains(col->uid()) || !m_rows->contains(row->uid()))
        return;

    if (m_db.updateValue(col,row,value)){
        const int updateIdx = m_rows->updateValue(col,row,value);

        doRecomputeRow(updateIdx);
        if (m_containsComplexColumns){
            const int rowCount = m_rows->count();
            for (int curIdx = updateIdx+1; curIdx < rowCount; curIdx++)
                doRecomputeRow(curIdx);
        }
    }
}

void Timeseries::setRank(const int rank){
    if (rank == m_rank)
        return;

    m_rank = rank;
    emit rankChanged();
}

void Timeseries::setTitle(const QString &title){
    if (title == m_title)
        return;

    m_title = title;
    emit titleChanged();
}

void Timeseries::insertColumn(DataColumn *col){
    if (m_db.insertColumn(col)){
        col->setParent(this);
        m_columns->insertColumn(col);

        switch (col->type()) {
        case DataColumn::GRADIENT:
        case DataColumn::AGGREGATION:
        case DataColumn::AVERAGE:
            m_containsComplexColumns = true;
            break;
        }
    }
}

void Timeseries::updateColumn(DataColumn *newCol){
    DataColumn *col = m_columns->get(newCol->uid());
    if (!col) {
        qWarning() << prompt("updateColumn()") << "could not find column with uid " << newCol->uid();
        return;
    }

    // copy properties to existing column
    if (m_db.updateColumn(newCol)){
        col->setIdx(newCol->idx());
        col->setTitle(newCol->title());
        col->setUnit(newCol->unit());
        col->setUsePreviousAsDefault(newCol->usePreviousAsDefault());
        col->setAllowNegative(newCol->allowNegative());
        col->setDigits(newCol->digits());
        col->setPrecision(newCol->precision());
        col->setBaseColumn1(newCol->baseColumn1());
        col->setBaseColumn2(newCol->baseColumn2());
        col->setReferenceTime(newCol->referenceTime());

        m_columns->resort();
    }
}

void Timeseries::removeColumn(const uint uid){
    if (m_db.removeColumn(uid))
        m_columns->removeColumn(uid);
}

void Timeseries::refreshValues(){
    for (int idx=0; idx<m_rows->count(); idx++)
        doRecomputeRow(idx);
}

void Timeseries::setDataColumnWidth(int dataColumnWidth){
    if (m_dataColumnWidth == dataColumnWidth)
        return;

    m_dataColumnWidth = dataColumnWidth;
    emit dataColumnWidthChanged();
}

void Timeseries::doInsertRow(DataRow *row){
    // insert row to model
    const int insertIdx = m_rows->insert(row);

    // compute derived columns - repeat loop until no new column was computed
    bool newColInserted = true;
    while(newColInserted){
        newColInserted = false;
        for (DataColumn *col : m_columns->list()){
            // skip column if already processed
            if (row->contains(col))
                continue;

            // process column
            switch (col->type()) {
            case DataColumn::SUM:
                if (doComputeSumColumn(row,col))
                    newColInserted = true;
                break;
            case DataColumn::DIFFERENCE:
                if (doComputeDifferenceColumn(row,col))
                    newColInserted = true;
                break;
            case DataColumn::PRODUCT:
                if (doComputeDifferenceColumn(row,col))
                    newColInserted = true;
                break;
            case DataColumn::QUOTIENT:
                if (doComputeQuotientColumn(row,col))
                    newColInserted = true;
                break;
            case DataColumn::AGGREGATION:
                if (doComputeAggregationColumn(row,col))
                    newColInserted = true;
                break;
            case DataColumn::AVERAGE:
                if (doComputeAverageColumn(row,col))
                    newColInserted = true;
                break;
            case DataColumn::GRADIENT:
                if (doComputeGradientColumn(row,col,insertIdx))
                    newColInserted = true;
                break;
            }
        }
    }

    // recompute all following columns
    if (m_containsComplexColumns)
        for (int curIdx = insertIdx+1; curIdx<m_rows->count(); curIdx++)
            doRecomputeRow(curIdx);

    // notify QML to refresh UI
    if (m_isSelected)
        m_rows->valuesChanged(insertIdx);

    // update beforeCountsMap
    const int rowYear  = row->date().year();
    const int rowMonth = row->date().month();
    const QDate curMonth = QDate(rowYear,rowMonth,2);
    if (m_monthlyBeforeCounts.isEmpty()){
        // initialise map
        m_monthlyBeforeCounts[curMonth] = 0;
        m_monthlyBeforeCounts[curMonth.addMonths(1)] = 1;
    } else {
        const QDate firstMonthInMap = m_monthlyBeforeCounts.firstKey();
        const QDate lastMonthInMap  = m_monthlyBeforeCounts.lastKey();
        if (curMonth < firstMonthInMap){
            for (auto it : m_monthlyBeforeCounts)
                it += 1;
            for (QDate m = curMonth.addMonths(1); m < firstMonthInMap; m = m.addMonths(1))
                m_monthlyBeforeCounts[m] = 1;
            m_monthlyBeforeCounts[curMonth] = 0;
        } else if (curMonth >= lastMonthInMap) {
            const int totalCount = m_rows->count();
            for (QDate m = lastMonthInMap.addMonths(1); m<=curMonth; m = m.addMonths(1))
                m_monthlyBeforeCounts[m] = totalCount-1;
            m_monthlyBeforeCounts[curMonth.addMonths(1)] = totalCount;
        } else {
            for (auto it = m_monthlyBeforeCounts.begin(); it != m_monthlyBeforeCounts.end(); it++)
                if (it.key() > curMonth)
                    it.value() += 1;
        }
    }
}

void Timeseries::doRecomputeRow(const int idx){
    DataRow *row = m_rows->at(idx);
    if (!row)
        return;

    DataRow *newRow = new DataRow;
    newRow->setUid(row->uid());
    newRow->setDate(row->date());

    // first fetch all raw data and store them in new Cols
    for (DataColumn *col : m_columns->list())
        if (col->type() == DataColumn::RAWDATA)
            newRow->insertValue(col,row->valueAt(col));

    // next compute all derived columns
    bool newColInserted = true;
    while(newColInserted){
        newColInserted = false;
        for (DataColumn *col : m_columns->list()){
            // skip column if already processed
            if (newRow->contains(col))
                continue;

            // process column
            switch (col->type()) {
            case DataColumn::SUM:
                if (doComputeSumColumn(newRow,col))
                    newColInserted = true;
                break;
            case DataColumn::DIFFERENCE:
                if (doComputeDifferenceColumn(newRow,col))
                    newColInserted = true;
                break;
            case DataColumn::PRODUCT:
                if (doComputeDifferenceColumn(newRow,col))
                    newColInserted = true;
                break;
            case DataColumn::QUOTIENT:
                if (doComputeQuotientColumn(newRow,col))
                    newColInserted = true;
                break;
            case DataColumn::AGGREGATION:
                if (doComputeAggregationColumn(newRow,col))
                    newColInserted = true;
                break;
            case DataColumn::AVERAGE:
                if (doComputeAverageColumn(newRow,col))
                    newColInserted = true;
                break;
            case DataColumn::GRADIENT:
                if (doComputeGradientColumn(newRow,col,idx))
                    newColInserted = true;
                break;
            }
        }
    }

    // sync row with newRow & delete temporary newRow
    row->setColumns(newRow->columns()->list());
    m_rows->valuesChanged(idx);
    delete newRow;
}

bool Timeseries::doComputeSumColumn(DataRow *row, DataColumn *col){
    DataColumn *baseCol1 = m_columns->getByIdx(col->baseColumn1());
    DataColumn *baseCol2 = m_columns->getByIdx(col->baseColumn2());
    if (!baseCol1 || !baseCol2){
        qWarning() << prompt("doComputeSumColumn()") << "Got a column with a non-existing base column!";
        return false;
    }

    if (row->contains(baseCol1) && row->contains(baseCol2)){
        const qreal value1 = row->valueAt(baseCol1->uid());
        const qreal value2 = row->valueAt(baseCol2->uid());
        row->insertValue(col,value1 + value2);
        return true;
    }
    return false;
}

bool Timeseries::doComputeDifferenceColumn(DataRow *row, DataColumn *col){
    DataColumn *baseCol1 = m_columns->getByIdx(col->baseColumn1());
    DataColumn *baseCol2 = m_columns->getByIdx(col->baseColumn2());
    if (!baseCol1 || !baseCol2){
        qWarning() << prompt("doComputeDifferenceColumn()") << "Got a column with a non-existing base column!";
        return false;
    }

    if (row->contains(baseCol1) && row->contains(baseCol2)){
        const qreal value1 = row->valueAt(baseCol1->uid());
        const qreal value2 = row->valueAt(baseCol2->uid());
        row->insertValue(col,value1 - value2);
        return true;
    }
    return false;
}

bool Timeseries::doComputeProductColumn(DataRow *row, DataColumn *col){
    DataColumn *baseCol1 = m_columns->getByIdx(col->baseColumn1());
    DataColumn *baseCol2 = m_columns->getByIdx(col->baseColumn2());
    if (!baseCol1 || !baseCol2){
        qWarning() << prompt("doComputeProductColumn()") << "Got a column with a non-existing base column!";
        return false;
    }

    if (row->contains(baseCol1) && row->contains(baseCol2)){
        const qreal value1 = row->valueAt(baseCol1->uid());
        const qreal value2 = row->valueAt(baseCol2->uid());
        row->insertValue(col,value1 * value2);
        return true;
    }
    return false;
}

bool Timeseries::doComputeQuotientColumn(DataRow *row, DataColumn *col){
    DataColumn *baseCol1 = m_columns->getByIdx(col->baseColumn1());
    DataColumn *baseCol2 = m_columns->getByIdx(col->baseColumn2());
    if (!baseCol1 || !baseCol2){
        qWarning() << prompt("doComputeDifferenceColumn()") << "Found a column with a non-existing base column!";
        return false;
    }

    if (row->contains(baseCol1) && row->contains(baseCol2)){
        const qreal value1 = row->valueAt(baseCol1->uid());
        const qreal value2 = row->valueAt(baseCol2->uid());
        if (value2 != 0) {
            row->insertValue(col,value1 / value2);
            return true;
        } else {
            row->removeColumn(col);
            return false;
        }
    }
    return false;
}

bool Timeseries::doComputeGradientColumn(DataRow *row, DataColumn *col, const int insertIdx){
    DataColumn *baseCol1 = m_columns->getByIdx(col->baseColumn1());
    if (!baseCol1){
        qWarning() << prompt("doComputeGradientColumn()") << "Got a column with a non-existing base column!";
        return false;
    }

    // if base column is empty, also derived column should stay empty
    if (!row->contains(baseCol1)){
        row->removeColumn(col);
        return false;
    }

    const DataColumn::GradientMode mode = col->gradientMode();

    const qreal lastBaseValue = m_rows->valueAt(insertIdx-1,baseCol1);
    const qreal curBaseValue  = row->valueAt(baseCol1);
    const qreal baseValueDiff = curBaseValue - lastBaseValue;

    if (mode == DataColumn::GRADIENT_ABSOLUTE){
        row->insertValue(col,baseValueDiff);
        return true;
    }
    if (mode == DataColumn::GRADIENT_PERDAY) {
        // keep column empty if insert at begin of list
        if (insertIdx > 0){
            const qint64 dayDiff = m_rows->dateAt(insertIdx-1).daysTo(m_rows->dateAt(insertIdx));
            if (dayDiff != 0){
                row->insertValue(col,baseValueDiff/dayDiff);
                return true;
            }
        }
        row->removeColumn(col);
        return false;
    }

    qWarning().noquote() << prompt("doComputeGradientColumn()") << "Found an invalid gradient column" << col->uid();
    return false;
}

bool Timeseries::doComputeAggregationColumn(DataRow *row, DataColumn *col){
    DataColumn *baseCol1 = m_columns->getByIdx(col->baseColumn1());
    if (!baseCol1){
        qWarning() << prompt("doComputeAggregationColumn()") << "Got a column with a non-existing base column!";
        return false;
    }

    // if base column is empty, also derived column should stay empty
    if (!row->contains(baseCol1)){
        row->removeColumn(col);
        return false;
    }

    const QDate firstDate = col->referenceTime() > 0 ? row->date().addDays(1-col->referenceTime()) : QDate();
    const QDate lastDate = row->date();
    const QList<qreal> values = m_rows->valuesInTimeRange(baseCol1,firstDate,lastDate);

    qreal sum = 0.;
    for (const qreal val : values)
        sum += val;

    row->insertValue(col,sum);
    return true;
}

bool Timeseries::doComputeAverageColumn(DataRow *row, DataColumn *col){
    DataColumn *baseCol1 = m_columns->getByIdx(col->baseColumn1());
    if (!baseCol1){
        qWarning() << prompt("doComputeAverageColumn()") << "Got a column with a non-existing base column!";
        return false;
    }

    // if base column is empty, also derived column should stay empty
    if (!row->contains(baseCol1)){
        row->removeColumn(col);
        return false;
    }

    const QDate firstDate = col->referenceTime() > 0 ? row->date().addDays(1-col->referenceTime()) : QDate();
    const QDate lastDate = row->date();
    const QList<qreal> values = m_rows->valuesInTimeRange(baseCol1,firstDate,lastDate);

    // if there are no values, there is no average
    if (values.isEmpty()){
        row->removeColumn(col);
        return false;
    }

    qreal sum = 0.;
    for (const qreal val : values)
        sum += val;

    row->insertValue(col,sum/values.size());
    return true;
}
