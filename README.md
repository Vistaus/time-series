# Time Series

This app is designed to easily record time series data on your phone.
The data can be exported as CSV files to further process them anywhere else.

## Translations

Currently, the app is available in the following languages:

* English
* German

Additional translations are very welcome and can be created by loading the file
**po/time-series.matdahl.pot** with a translation tool like (Poedit)[https://poedit.net/]
and adding the resulting *.po* file to the **po** directory of this repository.

## License

Copyright (C) 2024  Matthias Dahlmanns

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY
QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
