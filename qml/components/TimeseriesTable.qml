/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

import TSCore 1.0

Item {
    id: root

    property var ts: TSBackend.timeseries.selected

    property color headerColor: colors.indexColor > 0 ? colors.currentHeader : theme.palette.normal.foreground
    property int   headerHeight: units.gu(6)
    property int   rowHeight: units.gu(5)

    property real backgroundOpacityHeader: 0.6
    property real backgroundOpacityDate:   0.4

    readonly property int lowestVisibleIndex:  Math.floor(dataRows.contentY/rowHeight)
    readonly property int highestVisibleIndex: lowestVisibleIndex + Math.ceil(dataRows.height/rowHeight)
    readonly property int contentHeight: (ts.rows.count+2)*rowHeight

    readonly property bool isAtTop: dataRows.contentY <= 0
    readonly property bool isAtEnd: dataRows.contentY >= contentHeight - dataRows.height

    Component.onCompleted: toEnd()

    function bringIndexToTop(idx){
        dataRows.contentY = Math.min(contentHeight - dataRows.height,idx*rowHeight)
    }

    function toTop(){
        dataRows.contentY = 0
    }

    function toEnd(){
        dataRows.contentY = Math.max(0,contentHeight - dataRows.height)
    }


    TimeseriesTableCell {
        id: dateHeaderCell
        height: headerHeight
        width: ts.dataColumnWidth
        backgroundColor: root.headerColor
        backgroundOpacity: root.backgroundOpacityHeader

        Label{
            anchors.verticalCenter: parent.verticalCenter
            x: units.gu(2)
            font.bold: true
            text: i18n.tr("Date")
            onWidthChanged: {
                if (ts.dataColumnWidth < width + units.gu(4))
                    ts.dataColumnWidth = width + units.gu(4)
            }
        }
    }

    Flickable{
        id: headerCells
        anchors{
            left: dateHeaderCell.right
            right: parent.right
        }
        height: root.headerHeight
        clip: true

        contentWidth: Math.max(width,headerCellsRow.width + units.gu(2))

        Row{
            id: headerCellsRow
            height: root.headerHeight

            Repeater{
                model: root.ts.columns
                delegate: TimeseriesTableCell{
                    height: root.headerHeight
                    width: dataColumn.width
                    backgroundColor: root.headerColor
                    backgroundOpacity: root.backgroundOpacityHeader

                    Label{
                        anchors.centerIn: parent
                        font.bold: true
                        text: dataColumn.title
                        onWidthChanged: {
                            if (dataColumn.width < width + units.gu(2))
                                dataColumn.width = width + units.gu(2)
                        }
                    }
                }
            }
        }
    }

    Flickable{
        id: dataRows
        anchors{
            top: dateHeaderCell.bottom
            bottom: parent.bottom
        }
        clip: true
        width: root.width
        contentHeight: Math.max(root.contentHeight,height)

        Repeater{
            id: repeaterRows
            model: ts.rows
            delegate: Loader{
                y: index * root.rowHeight
                active: index >= root.lowestVisibleIndex-1 &&
                        index <= root.highestVisibleIndex+1
                property int rowIndex: index
                sourceComponent: Component{
                    ListItem{
                        id: dataRow
                        height: root.rowHeight
                        width: root.width
                        divider.visible: false

                        leadingActions: ListItemActions{
                            actions: [
                                Action{
                                    iconName: "delete"
                                    name: i18n.tr("Delete")
                                    onTriggered: ts.removeRow(modelRow)
                                }
                            ]
                        }

                        // Date cell
                        TimeseriesTableCell{
                            id: dataDateCell
                            width: root.ts.dataColumnWidth
                            height: root.rowHeight
                            backgroundColor: root.headerColor
                            backgroundOpacity: root.backgroundOpacityDate + (rowIndex%2 === 0 ? 0.05 : 0)

                            Column{
                                anchors.centerIn: parent
                                width: Math.max(lbDate.width,lbYear.width)
                                onWidthChanged: {
                                    if (root.ts.dataColumnWidth < width + units.gu(4))
                                        root.ts.dataColumnWidth = width + units.gu(4)
                                }

                                Label{
                                    id: lbDate
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    text: Qt.formatDate(modelRow.date,"d MMM")
                                }

                                Label{
                                    id: lbYear
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    text: Qt.formatDate(modelRow.date,"yyyy")
                                    textSize: Label.Small
                                }
                            }
                        } // dataDateCell

                        // columns
                        Flickable{
                            id: flickableColumnsRow
                            anchors{
                                left: dataDateCell.right
                                right: parent.right
                            }
                            height: root.rowHeight
                            clip: true
                            contentWidth: Math.max(width,columnsRow.width)

                            Binding{
                                target: flickableColumnsRow
                                property: "contentX"
                                value: headerCells.contentX
                                when: headerCells.moving
                            }

                            Row{
                                id: columnsRow
                                height: root.rowHeight
                                readonly property int rowIdx: rowIndex

                                Repeater{
                                    model: root.ts.columns
                                    delegate: TimeseriesTableCell{
                                        id: dataCell
                                        height: root.rowHeight
                                        width: dataColumn.width

                                        readonly property var dataCol: dataColumn
                                        backgroundColor: theme.palette.normal.base
                                        backgroundOpacity: (dataColumn.type !== DataColumn.RAWDATA ? 0.2 : 0.1)
                                                         + (columnsRow.rowIdx%2 === 0 ? 0.1 : 0.0)

                                        Row{
                                            id: labelRow
                                            anchors{
                                                verticalCenter: parent.verticalCenter
                                                right: parent.right
                                                rightMargin: units.gu(1)
                                            }
                                            height: units.gu(2)
                                            onWidthChanged: {
                                                if (dataColumn.width < width + units.gu(2))
                                                    dataColumn.width = width + units.gu(2)
                                            }

                                            Label{
                                                id: lbValue
                                                anchors. verticalCenter: parent.verticalCenter
                                                text: (modelRow.contains(dataColumn.uid) ? dataColumn.formatValue(modelRow.valueAt(dataColumn.uid)) : "-")
                                                    + modelValueChanges // bind property to refresh when a value has changed
                                            }

                                            Label{
                                                id: lbUnit
                                                anchors. verticalCenter: parent.verticalCenter
                                                color: theme.palette.normal.base
                                                text: (modelRow.contains(dataColumn.uid) && dataColumn.unit.length>0 ? " "+dataColumn.unit : "")
                                                    + modelValueChanges // bind property to refresh when a value has changed
                                            }
                                        }

                                        MouseArea{
                                            anchors.fill: parent
                                            onClicked: valueEditPopover.open(this)
                                            enabled: dataCell.dataCol.type === DataColumn.RAWDATA
                                        }

                                        DataColumnValueEditPopover{
                                            id: valueEditPopover
                                            dataColumn: dataCell.dataCol
                                            value: dataCell.dataCol.type === DataColumn.RAWDATA ? modelRow.valueAt(dataCell.dataCol.uid) : 0
                                            onValueChanged: {
                                                if (isOpen && dataCell.dataCol.type === DataColumn.RAWDATA)
                                                    root.ts.updateValue(dataColumn,modelRow,value)
                                            }
                                        } // valueEditPopover
                                    }
                                }
                            } // columnsRow
                        } // flickableColumnsRow
                    } // dataRow
                } // sourceComponent
            } // delegate
        } // repeaterRows

        Button{
            anchors.horizontalCenter: parent.horizontalCenter
            y: ts.rows.count*root.rowHeight + units.gu(0.5)
            width: 0.6* root.width
            height: root.rowHeight - units.gu(1)
            color: theme.palette.normal.positive
            Icon{
                anchors.centerIn: parent
                height: parent.height-units.gu(2)
                name:"add"
                color: theme.palette.normal.positiveText
            }
            onClicked: bottomEdgeNewDataRow.show(ts.uid)
        }
    } // dataRows
} // root
