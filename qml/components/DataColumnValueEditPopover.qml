/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Pickers 1.3

Item {
    id: root

    property var dataColumn
    property real value

    property bool isOpen: false

    function open(caller){
        PopupUtils.open(popoverComponent,caller)
        isOpen = true
    }

    Component{
        id: popoverComponent
        Popover{
            id: popover
            contentWidth: popoverContent.width

            Component.onCompleted: {
                var absVal = Math.abs(root.value)

                var digi = new Array(root.dataColumn.digits)
                for (var i=0; i<digi.length; i++)
                    digi[i] = Math.floor(Math.pow(10,-i) * absVal) % 10
                digits = digi

                var prec = new Array(root.dataColumn.precision)
                for (var j=0; j<prec.length; j++)
                    prec[j] = Math.floor(Math.pow(10,j+1) * absVal) % 10
                precision = prec
            }

            Component.onDestruction: {
                root.isOpen = false
            }

            function updateValue(){
                const powerOffset = precision.length
                var val = 0
                for (var i=0; i<digits.length; i++)
                    val += Math.pow(10,i+powerOffset)*digits[i]
                for (var j=0; j<precision.length; j++)
                    val += Math.pow(10,powerOffset-1-j)*precision[j]
                root.value = val * Math.pow(10,-powerOffset)
            }

            property var digits: []

            property var precision: []

            Row{
                id: popoverContent
                padding: units.gu(2)
                spacing: units.gu(0.5)
                height: signPicker.height + 2*padding

                // sign
                Picker{
                    id: signPicker
                    width: units.gu(4)
                    model: ["+","-"]
                    circular: false
                    visible: root.dataColumn.allowNegative
                    delegate: PickerDelegate{
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: modelData
                        }
                    }
                    Component.onCompleted: selectedIndex = root.value <0 ? 1 : 0
                    onSelectedIndexChanged: {
                        root.value = (selectedIndex === 0 ? 1 : -1) * Math.abs(root.value)
                    }
                }

                // digits
                Repeater{
                    id: repDigits
                    model: root.dataColumn.digits
                    delegate: Picker{
                        id: digitPicker
                        property int power: root.dataColumn.digits - 1 - index
                        width: units.gu(4)
                        circular: true
                        model: 10
                        delegate: PickerDelegate{
                            Label {
                                anchors.horizontalCenter: parent.horizontalCenter
                                text: index
                            }
                        }
                        property bool initialised: false
                        property int digitsLength: popover.digits.length
                        onDigitsLengthChanged: {
                            if (digitsLength > power && !initialised){
                                selectedIndex = popover.digits[power]
                                initialised = true
                            }
                        }
                        onSelectedIndexChanged: {
                            popover.digits[power] = selectedIndex
                            popover.updateValue()
                        }
                    }
                }

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: Qt.locale().decimalPoint
                    visible: root.dataColumn.precision > 0
                }

                // precision
                Repeater{
                    id: repPrecision
                    model: root.dataColumn.precision
                    delegate: Picker{
                        property int power: index
                        width: units.gu(4)
                        circular: true
                        model: 10
                        delegate: PickerDelegate{
                            Label {
                                anchors.horizontalCenter: parent.horizontalCenter
                                text: modelData
                            }
                        }
                        property bool initialised: false
                        property int precisionLength: popover.precision.length
                        onPrecisionLengthChanged: {
                            if (precisionLength > power && !initialised){
                                selectedIndex = popover.precision[power]
                                initialised = true
                            }
                        }
                        onSelectedIndexChanged: {
                            popover.precision[power] = selectedIndex
                            popover.updateValue()
                        }
                    }
                }

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: root.dataColumn.unit
                }
            }
        }
    }
}
