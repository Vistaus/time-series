/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Button {
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width

    property var  dataCol: dataColumn
    property real value

    text: value.toLocaleString(Qt.locale(),'f',dataCol.precision) + (dataCol.unit.length>0 ? " "+dataCol.unit : "")
    onClicked: popover.open(root)

    DataColumnValueEditPopover{
        id: popover
        dataColumn: root.dataCol
        Binding{
            target: popover; property: "value"
            value: root.value
        }
        onValueChanged: root.value = value
    }
}
