/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

import TSCore 1.0

Item {
    id: root
    height: units.gu(4)
    width: units.gu(8)

    Label{
        id: rawDataItem
        visible: dataColumn.type === DataColumn.RAWDATA
        anchors.centerIn: parent
        text: utils.columnTitle(DataColumn.RAWDATA)
        font.italic: true
    }

    Row{
        id: aggregationItem
        visible: dataColumn.type === DataColumn.AGGREGATION
        anchors.centerIn: parent
        spacing: units.gu(0.5)
        height: canvasAggregation.height

        Canvas{
            id: canvasAggregation
            height: units.gu(3)
            width: units.gu(2)
            onPaint: {
                var ctx = getContext("2d");
                ctx.strokeStyle = theme.palette.normal.backgroundText
                ctx.lineWidth   = units.gu(0.5)

                ctx.beginPath()
                ctx.moveTo(width,units.gu(0.5))
                ctx.lineTo(width,0)
                ctx.lineTo(0,0)
                ctx.lineTo(width/2,height/2)
                ctx.lineTo(0,height)
                ctx.lineTo(width,height)
                ctx.lineTo(width,height-units.gu(0.5))
                ctx.stroke()
            }
        }
        DataColumnIndexIcon{
            size: units.gu(3)
            text: dataColumn.baseColumn1
        }
    }

    Row{
        id: gradientItem
        visible: dataColumn.type === DataColumn.GRADIENT
        anchors.centerIn: parent
        spacing: units.gu(0.5)
        height: canvasGradient.height

        Canvas{
            id: canvasGradient
            height: units.gu(2.5)
            width: units.gu(2)
            onPaint: {
                var ctx = getContext("2d");
                ctx.strokeStyle = theme.palette.normal.backgroundText
                ctx.lineWidth   = units.gu(0.4)

                ctx.beginPath()
                ctx.moveTo(0,0)
                ctx.lineTo(width,0)
                ctx.lineTo(width/2,height)
                ctx.lineTo(0,0)
                ctx.stroke()
            }
        }
        DataColumnIndexIcon{
            size: units.gu(3)
            text: dataColumn.baseColumn1
        }
    }

    Column{
        id: averageItem
        visible: dataColumn.type === DataColumn.AVERAGE
        anchors.centerIn: parent
        spacing: units.gu(0.25)
        width: units.gu(4)

        Rectangle{
            width: units.gu(4)
            height: units.gu(0.25)
            color: theme.palette.normal.backgroundText
        }

        DataColumnIndexIcon{
            anchors.horizontalCenter: parent.horizontalCenter
            size: units.gu(3)
            text: dataColumn.baseColumn1
        }
    }


    Row{
        id: sumItem
        visible: dataColumn.type === DataColumn.SUM
        anchors.centerIn: parent
        spacing: units.gu(0.5)

        DataColumnIndexIcon{
            size: units.gu(3)
            text: dataColumn.baseColumn1
        }

        Item{
            id: plusCharacter
            anchors.verticalCenter: parent.verticalCenter
            width: units.gu(2)
            height: width

            Rectangle{
                anchors.verticalCenter: parent.verticalCenter
                height: units.gu(0.5)
                width:  parent.width
                radius: height/2
                color: theme.palette.normal.backgroundText
            }

            Rectangle{
                anchors.horizontalCenter: parent.horizontalCenter
                height: parent.height
                width:  units.gu(0.5)
                radius: width/2
                color: theme.palette.normal.backgroundText
            }
        }

        DataColumnIndexIcon{
            size: units.gu(3)
            text: dataColumn.baseColumn2
        }
    }

    Row{
        id: differenceItem
        visible: dataColumn.type === DataColumn.DIFFERENCE
        anchors.centerIn: parent
        spacing: units.gu(0.5)

        DataColumnIndexIcon{
            size: units.gu(3)
            text: dataColumn.baseColumn1
        }

        Rectangle{
            anchors.verticalCenter: parent.verticalCenter
            height: units.gu(0.5)
            width:  units.gu(2)
            radius: height/2
            color: theme.palette.normal.backgroundText
        }

        DataColumnIndexIcon{
            size: units.gu(3)
            text: dataColumn.baseColumn2
        }
    }

    Row{
        id: productItem
        visible: dataColumn.type === DataColumn.PRODUCT
        anchors.centerIn: parent
        spacing: units.gu(0.5)

        DataColumnIndexIcon{
            size: units.gu(3)
            text: dataColumn.baseColumn1
        }

        Rectangle{
            anchors.verticalCenter: parent.verticalCenter
            height: units.gu(0.75)
            width: height
            radius: height/2
            color: theme.palette.normal.backgroundText
        }

        DataColumnIndexIcon{
            size: units.gu(3)
            text: dataColumn.baseColumn2
        }
    }

    Column{
        id: quotientItem
        visible: dataColumn.type === DataColumn.QUOTIENT
        anchors.centerIn: parent
        width: units.gu(5)
        spacing: units.gu(0.125)

        DataColumnIndexIcon{
            anchors.horizontalCenter: parent.horizontalCenter
            size: units.gu(3)
            text: dataColumn.baseColumn1
        }

        Rectangle{
            id: separator
            height: units.gu(0.25)
            width: parent.width
            color: theme.palette.normal.backgroundText
        }

        DataColumnIndexIcon{
            anchors.horizontalCenter: parent.horizontalCenter
            size: units.gu(3)
            text: dataColumn.baseColumn2
        }
    }
}
