/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Pickers 1.3

Button {
    id: root

    property date date: new Date()

    text: date.toLocaleDateString()

    onClicked: PopupUtils.open(popoverComponent,root)

    Component{
        id: popoverComponent
        Popover{
            id: popover
            Column{
                id: popoverContent
                padding: units.gu(2)

                DatePicker{
                    date: root.date
                    onDateChanged: root.date = date
                    mode: "Years|Months|Days"
                    minimum: new Date(new Date().getFullYear()-20,1,1)
                    maximum: new Date(new Date().getFullYear()+20,12,31)
                }
            }
        }
    }
}
