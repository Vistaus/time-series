/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

import TSCore 1.0

Column {
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width

    spacing: units.gu(2)

    // must be set to a DataColumn object
    property var dataColumn

    // flag to highlight, that the column is frozen
    property bool freeze: false

    property int maxDigits: 6
    property int maxPrecision: 6

    property int controlsHeight: units.gu(4)
    property int previewHeight:  inputUnit.implicitHeight

    property int innerSpacing: units.gu(1)
    property int signAreaWidth: swNegativeValues.implicitWidth
    property int digitsColumnWidth: (width - unitArea.width - signAreaWidth - 5*innerSpacing)/2

    readonly property bool canEditSign:   dataColumn.type === DataColumn.RAWDATA
    readonly property bool canEditDigits: dataColumn.type === DataColumn.RAWDATA

    Label{
        text: i18n.tr("Column visualisation")
    }

    Item{
        id: content
        width: root.width
        height: digitsArea.height

        Item{
            id: digitsArea
            height: 2*root.controlsHeight + root.previewHeight + 2*root.innerSpacing
            anchors{
                left: parent.left
                right: unitArea.left
                rightMargin: units.gu(1)
            }

            Item{
                id: topControls
                x: root.innerSpacing
                height: root.controlsHeight

                Item{
                    height: parent.height
                    width: root.signAreaWidth
                    visible: root.canEditSign

                    Switch{
                        id: swNegativeValues
                        anchors.centerIn: parent
                        checked: root.dataColumn.allowNegative
                        onCheckedChanged: {
                            if (!root.freeze)
                                root.dataColumn.allowNegative = checked
                        }
                    }
                }

                Button{
                    id: btIncDigits
                    x: root.signAreaWidth + root.innerSpacing
                    height: topControls.height
                    width: root.digitsColumnWidth
                    color: theme.palette.normal.positive
                    text: "+"
                    visible: root.canEditDigits

                    enabled: root.dataColumn.digits < root.maxDigits && !root.freeze
                    onClicked: root.dataColumn.digits += 1
                }

                Button{
                    id: btIncPrecision
                    x: root.signAreaWidth + root.digitsColumnWidth + 2*root.innerSpacing
                    height: topControls.height
                    width: root.digitsColumnWidth
                    color: theme.palette.normal.positive
                    text: "+"

                    enabled: root.dataColumn.precision < root.maxPrecision && !root.freeze
                    onClicked: root.dataColumn.precision += 1
                }
            }

            Item{
                id: itemPreview
                y: root.controlsHeight + root.innerSpacing
                height: root.previewHeight
                width: root.width - unitArea.width - root.innerSpacing

                Rectangle{
                    id: previewBackground
                    anchors.fill: parent
                    radius: units.gu(1)
                    color: theme.palette.normal.base
                }

                Label{
                    id: lbSign
                    x: root.innerSpacing
                    anchors.verticalCenter: parent.verticalCenter
                    width: root.signAreaWidth
                    horizontalAlignment: Label.AlignHCenter
                    text: "-"
                    font.bold: true
                    textSize: Label.Large
                    enabled: root.dataColumn.allowNegative
                }

                Label{
                    id: lbDigitsDisabled
                    anchors{
                        verticalCenter: parent.verticalCenter
                        right: lbDigitsEnabled.left
                        leftMargin: units.gu(1)
                    }
                    text: new Array(root.maxDigits-root.dataColumn.digits+1).join("X");
                    enabled: false
                    visible: root.canEditDigits
                }
                Label{
                    id: lbDigitsEnabled
                    anchors{
                        verticalCenter: parent.verticalCenter
                        right: lbDecimalPoint.left
                    }
                    text: new Array((root.canEditDigits ? root.dataColumn.digits : root.maxDigits) + 1).join("X");
                }

                Label{
                    id: lbDecimalPoint
                    anchors.verticalCenter: parent.verticalCenter
                    x: root.signAreaWidth + root.digitsColumnWidth + 2*root.innerSpacing
                    width: root.innerSpacing
                    horizontalAlignment: Label.AlignHCenter
                    text: Qt.locale().decimalPoint
                    enabled: root.dataColumn.precision > 0
                }

                Label{
                    id: lbPrecisionEnabled
                    anchors{
                        verticalCenter: parent.verticalCenter
                        left: lbDecimalPoint.right
                    }
                    text: new Array(root.dataColumn.precision+1).join("X");
                }
                Label{
                    id: lbPrecisionDisabled
                    anchors{
                        verticalCenter: parent.verticalCenter
                        left: lbPrecisionEnabled.right
                    }
                    text: new Array(root.maxPrecision-root.dataColumn.precision+1).join("X");
                    enabled: false
                }
            }

            Item{
                id: bottomControls
                y: root.controlsHeight + root.previewHeight + 2*root.innerSpacing
                x: root.innerSpacing
                height: root.controlsHeight

                Button{
                    id: btDecDigits
                    x: root.signAreaWidth + root.innerSpacing
                    height: bottomControls.height
                    width: root.digitsColumnWidth
                    color: theme.palette.normal.negative
                    text: "-"
                    visible: root.canEditDigits

                    enabled: root.dataColumn.digits > 1 && !root.freeze
                    onClicked: root.dataColumn.digits -= 1
                }

                Button{
                    id: btDecPrecision
                    x: root.signAreaWidth + root.digitsColumnWidth + 2*root.innerSpacing
                    height: bottomControls.height
                    width: root.digitsColumnWidth
                    color: theme.palette.normal.negative
                    text: "-"

                    enabled: root.dataColumn.precision > 0 && !root.freeze
                    onClicked: root.dataColumn.precision -= 1
                }
            }
        }

        Column{
            id: unitArea
            anchors.right: content.right
            width: Math.max(lbUnit.width,inputUnit.width)
            spacing: root.innerSpacing

            Label{
                id: lbUnit
                anchors.horizontalCenter: parent.horizontalCenter
                height: root.controlsHeight
                verticalAlignment: Label.AlignVCenter
                text: i18n.tr("Unit")
            }

            TextField{
                id: inputUnit
                anchors.horizontalCenter: parent.horizontalCenter
                width: units.gu(7)
                hasClearButton: false
                placeholderText: "..."
                inputMethodHints: Qt.ImhNoPredictiveText
                text: root.dataColumn.unit
                onDisplayTextChanged: {
                    if (!root.freeze)
                        root.dataColumn.unit = displayText
                }
            }
        }
    }
}
