/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Item {
    id: root

    property alias backgroundColor: background.color
    property alias backgroundOpacity: background.opacity

    // background
    Rectangle{
        id: background
        anchors.fill: parent
    }

    // border
    Rectangle{
        id: border
        anchors.right: parent.right
        height: parent.height
        width: units.gu(0.25)
        color: theme.palette.normal.base
    }

}
