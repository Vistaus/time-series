/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

import "../dialogs"

import TSCore 1.0

ListItem{
    id: root
    height: units.gu(6)
    width: parent.contentWidth ? parent.contentWidth : parent.width
    divider.visible: false

    property alias dataColumnModel: editDialog.dataColumnModel
    property var dataCol: dataColumn
    property var dragParent

    Rectangle{
        anchors.fill: parent
        color: theme.palette.normal.base
        opacity: index%2 === 0 ? 0.1 : 0.15
    }


    leadingActions: ListItemActions{
        actions: [
            Action{
                iconName: "remove"
                name: i18n.tr("Remove")
                onTriggered: {
                    deleteDialog.deps = root.dataColumnModel.getDependentIndices(dataColumn.idx)
                    if (deleteDialog.deps.length > 0){
                        deleteDialog.show()
                    } else {
                        root.dataColumnModel.removeColumnAt(dataColumn.idx)
                    }
                }
            }

        ]
    }

    trailingActions: ListItemActions{
        actions: [
            Action{
                iconName: "edit"
                name: i18n.tr("Edit")
                onTriggered: editDialog.show()
            }

        ]
    }

    Item{
        id: content
        width: root.width
        height: root.height

        MouseArea{
            id: dragMouse
            height: parent.height
            width: units.gu(8)
            drag.target: content

            Icon{
                anchors.centerIn: parent
                height: units.gu(4)
                name: "grip-large"
                color: theme.palette.normal.backgroundText
            }
        }

        DataColumnIndexIcon{
            id: indexIcon
            anchors{
                verticalCenter: parent.verticalCenter
                left: dragMouse.right
            }
            size: units.gu(4)
            textSize: Label.Large
            textBold: true
            text: dataColumn.idx
        }

        Column{
            id: titleColumn
            anchors{
                verticalCenter: parent.verticalCenter
                left: indexIcon.right
                leftMargin: units.gu(1)
            }
            spacing: units.gu(0.5)

            Label{
                text: dataColumn.title
            }
            Label{
                color: theme.palette.normal.base
                text: (dataColumn.allowNegative ? "- " : "")
                      + (dataColumn.precision>0 ? Qt.locale().decimalPoint.padStart(dataColumn.digits+1,"X").padEnd(dataColumn.digits+1+dataColumn.precision,"X")
                                                : new Array(dataColumn.digits+1).join("X"))
                      + " <i>" + dataColumn.unit + "</i>"
            }
        }

        DataColumnTypeIcon{
            id: columnTypeItem
            anchors{
                verticalCenter: parent.verticalCenter
                right: parent.right
                rightMargin: units.gu(2)
            }
        }

        property int dragItemIndex: index

        states: [
            State {
                when: content.Drag.active
                ParentChange {
                    target: content
                    parent: root.dragParent
                }
            },
            State {
                when: !content.Drag.active
                AnchorChanges {
                    target: content
                    anchors.horizontalCenter: content.parent.horizontalCenter
                    anchors.verticalCenter: content.parent.verticalCenter
                }
            }
        ]
        Drag.active: dragMouse.drag.active
        Drag.hotSpot.x: content.width / 2
        Drag.hotSpot.y: content.height / 2
    }

    DropArea{
        anchors.fill: parent
        onEntered: {
            if (drag.source.dragItemIndex > index)
                root.dataColumnModel.swap(drag.source.dragItemIndex,index)
            else if (drag.source.dragItemIndex < index)
                root.dataColumnModel.swap(index,drag.source.dragItemIndex)
        }
    }

    DialogColumnEdit{
        id: editDialog
        dataColumn: root.dataCol
    }

    DialogColumnDelete{
        id: deleteDialog
        idx: dataColumn.idx
        dataColumnModel: root.dataColumnModel
    }
}
