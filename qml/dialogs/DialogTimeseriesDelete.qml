/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import TSCore 1.0

Item {
    id: root
    anchors.fill: parent

    // the time series to delete
    property var ts

    function show(ts){
        root.ts = ts
        PopupUtils.open(dialogComponent)
    }

    Component{
        id: dialogComponent
        Dialog{
            id: dialog
            // TRANSLATORS: time series is singular here
            title: i18n.tr("Delete time series")
            // TRANSLATORS: %1 refers to the title of the time series (singular) to delete
            text: i18n.tr("Do you want to permanently delete the time series %1?").arg("<br><br><b>%1%2").arg(root.ts ? root.ts.title : "UNDEFINED").arg("</b><br><br>") + "<br><br>" +
                  "(" + i18n.tr("This action can not be undone") + ")"

            Button{
                id: btCancel
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(dialog)
            }

            Button{
                id: btDelete
                color: theme.palette.normal.negative
                Label{
                    anchors.centerIn: parent
                    color: theme.palette.normal.negativeText
                    text: i18n.tr("Delete")
                }
                onClicked: {
                    TSBackend.timeseries.remove(root.ts.uid)
                    PopupUtils.close(dialog)
                }
            }
        }
    }
}
