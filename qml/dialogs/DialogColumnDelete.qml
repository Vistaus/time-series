/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import "../components"

import TSCore 1.0

Item {
    id: root

    property int idx: -1
    property var dataColumnModel
    property var deps: []


    function show(){
        PopupUtils.open(dialogComponent)
    }


    Component{
        id: dialogComponent
        Dialog{
            id: dialog
            title: i18n.tr("Delete column")

            Label{
                width: dialog.contentWidth
                wrapMode: Label.Wrap
                text: i18n.tr("This column has the following derived columns:")
            }

            Repeater{
                model: root.deps
                delegate: Row{
                    height: lbTitle.height
                    spacing: units.gu(1)

                    Rectangle{
                        anchors.verticalCenter: lbTitle.verticalCenter
                        width: units.gu(0.5)
                        height: width
                        radius: width/2
                        color: theme.palette.normal.base
                    }

                    Label{
                        id: lbTitle
                        font.bold: true
                        text: root.dataColumnModel.titleAt(modelData)
                    }
                }
            }

            Label{
                width: dialog.contentWidth
                wrapMode: Label.Wrap
                text: i18n.tr("Deleting this column will also delete all derived columns.")
            }

            Button{
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(dialog)
            }

            Button{
                text: i18n.tr("Delete")
                color: theme.palette.normal.negative
                onClicked: {
                    root.dataColumnModel.removeColumnAt(root.idx)
                    PopupUtils.close(dialog)
                }
            }
        }
    }
}
