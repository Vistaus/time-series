/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

import "../components"

import TSCore 1.0

Item {
    id: root
    anchors.fill: parent

    function show(){
        bottomEdge.commit()
    }

    BottomEdge {
        id: bottomEdge
        hint.status: BottomEdgeHint.Hidden
        onCollapseCompleted: contentItem.reset()

        contentComponent: Rectangle {
            id: bottomEdgeContent
            height: root.height
            width:  root.width
            color: colors.currentBackground

            function reset(){
                inputTitle.text = ""
                TSBackend.timeseries.newColumns.clear()
            }

            PageHeader{
                id: header
                StyleHints{backgroundColor: colors.currentHeader}
                // TRANSLATORS: time series is singular here
                title: i18n.tr("New time series")

                trailingActionBar.actions: [
                    Action{
                        iconName: "ok"
                        enabled: inputTitle.displayText.length > 0
                        onTriggered: {
                            TSBackend.timeseries.insert(inputTitle.displayText)
                            bottomEdge.collapse()
                        }
                    }
                ]
            }

            Flickable{
                id: flick
                anchors{
                    top: header.bottom
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                contentHeight: Math.max(height,col.height)

                Column{
                    id: col
                    width: flick.width
                    padding: units.gu(2)
                    spacing: units.gu(2)
                    property int contentWidth: width - 2*padding

                    Label{
                        text: i18n.tr("Name")
                    }
                    TextField{
                        id: inputTitle
                        width: col.contentWidth
                        placeholderText: i18n.tr("insert name ...")
                    }

                    Label{
                        text: i18n.tr("Columns")
                    }

                    Column{
                        width: col.contentWidth
                        Repeater{
                            id: repeaterColumns
                            model: TSBackend.timeseries.newColumns
                            delegate: ListItemDataColumn{
                                dataColumnModel: repeaterColumns.model
                                dragParent: bottomEdgeContent
                            }
                        }
                    }

                    Button{
                        id: btNewColumn
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: 0.6*col.contentWidth
                        onClicked: columnAddDialog.show()

                        Row{
                            anchors.centerIn: parent
                            height: units.gu(2)
                            spacing: units.gu(1)

                            Icon{
                                height: units.gu(2)
                                color: theme.palette.normal.baseText
                                name: "add"
                            }

                            Label{
                                color: theme.palette.normal.baseText
                                text: i18n.tr("Add column")
                            }
                        }
                    }
                }
            }

            DialogColumnAdd{
                id: columnAddDialog
                dataColumnModel: TSBackend.timeseries.newColumns
            }
        }
    }
}
