/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import MDTK.Manual 1.0

import TSCore 1.0

ManualDialog {
    id: root
    // TRANSLATORS: Time Series = the title of this app
    title: i18n.tr("Welcome to <i>Time Series</i>!")

    introduction: i18n.tr("This app is designed to easily record time series data on your phone. "+
                          "The data can be exported as CSV files to further process them anywhere else.")

    tableOfContentsDepth: 1

    ManualHeader{
        title: i18n.tr("Usage")
    }
    ManualParagraph{
        text: i18n.tr("You can create different time series on the start screen. "+
                      "For each time series, you can add an arbitrary number of columns.")
    }
    ManualParagraph{
        text: i18n.tr("There are columns that are filled with manually entered data and derived columns, "+
                      "which are automatically computed based on the values in other columns.")
    }
    ManualParagraph{
        text: i18n.tr("When swiping from the bottom edge of the screen, a panel appears to insert a new row into a time series.")
    }

    ManualHeader{
        title: i18n.tr("Column types")
    }
    ManualParagraph{
        text: i18n.tr("Currently, the following column types are available:")
    }

    ManualHeader{
        type: 2
        title: utils.columnTitle(DataColumn.RAWDATA)
    }
    ManualParagraph{
        text: i18n.tr("This is the basic type of columns which contain by manually entered values.")
    }

    ManualHeader{
        type: 2
        title: utils.columnTitle(DataColumn.GRADIENT)
    }
    ManualParagraph{
        text: i18n.tr("The change of an other column compared to its value in the preceding row. "+
                      "The gradient can be absolut change or normalised to the change per day.")
    }

    ManualHeader{
        type: 2
        title: utils.columnTitle(DataColumn.AGGREGATION)
    }
    ManualParagraph{
        text: i18n.tr("The sum of all preceding entries of an other column.")
    }
    ManualParagraph{
        text: i18n.tr("Optionally, the aggregation can be limited to a certain time range, e.g. to sum up all rows within the last week.")
    }

    ManualHeader{
        type: 2
        title: utils.columnTitle(DataColumn.AVERAGE)
    }
    ManualParagraph{
        text: i18n.tr("The average value over all preceding entries of an other column.")
    }
    ManualParagraph{
        text: i18n.tr("Optionally, the average can be limited to a certain time range, e.g. to get the average value over the last week.")
    }

    ManualHeader{
        type: 2
        title: i18n.tr("Combined columns")
    }
    ManualParagraph{
        text: i18n.tr("These columns are deduced from two other columns in the same row by performing one of the following operations:")
    }
    ManualListing{
        model: [
            // TRANSLATORS: the mathematical operation
            i18n.tr("Addition"),
            // TRANSLATORS: the mathematical operation
            i18n.tr("Substraction"),
            // TRANSLATORS: the mathematical operation
            i18n.tr("Multiplication"),
            // TRANSLATORS: the mathematical operation
            i18n.tr("Division")
        ]
    }

    ManualHeader{
        title: i18n.tr("Export")
    }
    ManualParagraph{
        text: i18n.tr("With the export feature, you can easily export your time series as a CSV file. "+
                      "You can export a time series by swiping the corresponding list item to the left and click on the \"share\" icon:")
    }
    Icon{
        anchors.horizontalCenter: parent.horizontalCenter
        height: units.gu(2)
        name: "share"
        color: theme.palette.normal.backgroundText
    }
    ManualParagraph{
        text: i18n.tr("When exporting a time series with derived columns, you can choose whether all values should be resolved or "+
                      "if derived columns should be exported as LibreOffice compatible formulas.")
    }

    ManualHeader{
        title: i18n.tr("Feedback")
    }
    ManualParagraph{
        text: i18n.tr("Do you have any feedback? Want to see the source code? Found a bug? Missing a feature?")
    }
    ManualParagraph{
        // TRANSLATORS: %1 contains a hyperlink with the displayed text "GitLab"
        text: i18n.tr("Feel free to get in touch on %1!").arg("<a href=\"https://gitlab.com/matdahl/time-series\">GitLab</a>")
        onLinkActivated: Qt.openUrlExternally(link)
    }
}
