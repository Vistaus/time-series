/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

import "../components"

import TSCore 1.0

Item {
    id: root
    anchors.fill: parent

    property int  tsUid: 0
    property bool openedWidthUid: false

    function show(tsUid){
        root.tsUid = tsUid
        root.openedWidthUid = true
        bottomEdge.commit()
    }

    BottomEdge {
        id: bottomEdge
        hint{
            status: root.enabled ? bottomEdge.showBottomEdgeHint ? BottomEdgeHint.Active : BottomEdgeHint.Inactive
                                 : BottomEdgeHint.Hidden
            iconName: "add"
            text: i18n.tr("New data row")
            onStatusChanged: {
                if (root.enabled && bottomEdge.showBottomEdgeHint)
                    hint.status = BottomEdgeHint.Active
            }
        }
        enabled: root.enabled
        readonly property bool showBottomEdgeHint: settings.showBottomEdgeHint
        onShowBottomEdgeHintChanged: hint.status = root.enabled ? showBottomEdgeHint ? BottomEdgeHint.Active : BottomEdgeHint.Inactive
                                                                : BottomEdgeHint.Hidden

        onCollapseCompleted: contentItem.reset()
        onCommitCompleted: {
            if (root.openedWidthUid)
                contentItem.selectTimeseries(root.tsUid)
            root.openedWidthUid = false
            contentItem.refresh()
        }

        contentComponent: Rectangle {
            id: bottomEdgeContent
            height: root.height
            width:  root.width
            color: colors.currentBackground

            property var dataRow: DataRow{}

            function reset(){
                repeaterColumns.reset()
                dataRow.columns.clear()
            }

            function selectTimeseries(tsUid){
                tsSelector.selectedIndex = tsSelector.model.indexOf(tsUid)
            }

            function refresh(){
                // force the column items to refresh
                const selectedIdx = tsSelector.selectedIndex
                tsSelector.selectedIndex = -1
                tsSelector.selectedIndex = selectedIdx
            }

            PageHeader{
                id: header
                StyleHints{backgroundColor: colors.currentHeader}
                title: i18n.tr("New data row")
            }

            TimeseriesSelector{
                id: tsSelector
                onSelectedTimeseriesChanged: bottomEdgeContent.dataRow.columns.clear()
            }

            Flickable{
                id: columnsArea
                anchors{
                    top: tsSelector.bottom
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                contentHeight: Math.max(height,columnsColumn.height)

                property int columnTitlesWidth: units.gu(10)

                Column{
                    id: columnsColumn
                    width: columnsArea.width
                    padding: units.gu(2)
                    spacing: units.gu(2)
                    readonly property int contentWidth: width - 2*padding

                    Item{
                        height: inputDate.height
                        width: columnsColumn.contentWidth

                        Label{
                            anchors.verticalCenter: parent.verticalCenter
                            text: i18n.tr("Date")
                            onWidthChanged: {
                                if (width > columnsArea.columnTitlesWidth)
                                    columnsArea.columnTitlesWidth = width
                            }
                        }

                        DateEditButton{
                            id: inputDate
                            anchors{
                                left: parent.left
                                right: parent.right
                                leftMargin: columnsArea.columnTitlesWidth + units.gu(2)
                            }
                            onDateChanged: bottomEdgeContent.dataRow.date = date
                        }
                    }

                    Rectangle{
                        width: columnsColumn.contentWidth
                        height: units.gu(0.125)
                        color: theme.palette.normal.foreground
                    }

                    Repeater{
                        id: repeaterColumns
                        model: tsSelector.selectedTimeseries ? tsSelector.selectedTimeseries.columns : 0

                        function reset(){
                            for (var i=0; i<children.length; i++)
                                children[i].reset()
                        }

                        delegate: Item{
                            height: btValue.height
                            width: columnsColumn.contentWidth
                            visible: dataColumn.type === DataColumn.RAWDATA

                            Component.onCompleted: reset()
                            function reset(){
                                if (dataColumn.type === DataColumn.RAWDATA)
                                    btValue.value = dataColumn.usePreviousAsDefault ? tsSelector.selectedTimeseries.rows.getLastValue(dataColumn.uid)
                                                                                    : dataColumn.defaultValue
                            }

                            Label{
                                anchors.verticalCenter: parent.verticalCenter
                                text: dataColumn.title
                                onWidthChanged: {
                                    if (width > columnsArea.columnTitlesWidth)
                                        columnsArea.columnTitlesWidth = width
                                }
                            }

                            DataColumnValueEditButton{
                                id: btValue
                                anchors{
                                    left: parent.left
                                    right: parent.right
                                    leftMargin: columnsArea.columnTitlesWidth + units.gu(2)
                                }
                                onValueChanged: bottomEdgeContent.dataRow.columns.insertValue(dataColumn,value)
                            }
                        }
                    }

                    Rectangle{
                        width: columnsColumn.contentWidth
                        height: units.gu(0.125)
                        color: theme.palette.normal.foreground
                    }

                    Button{
                        id: btInsert
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: 0.6*columnsColumn.width
                        color: theme.palette.normal.positive

                        Row{
                            anchors.centerIn: parent
                            spacing: units.gu(1)
                            Icon{
                                name: "ok"
                                height: units.gu(2)
                                color: theme.palette.normal.positiveText
                            }
                            Label{
                                text: i18n.tr("Insert row")
                                color: theme.palette.normal.positiveText
                            }
                        }

                        onClicked: {
                            tsSelector.selectedTimeseries.insertRow(bottomEdgeContent.dataRow)
                            bottomEdge.collapse()
                        }
                    }
                }
            }
        }
    }
}
