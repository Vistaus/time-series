/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import MDTK 1.0

import "../components"

import TSCore 1.0

ThemedFlickablePage{
    id: root
    title: i18n.tr("Select a time series")

    headerTrailingActionBar.actions: [
        Action{
            iconName: "settings"
            onTriggered: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
        }
    ]

    Repeater{
        model: TSBackend.timeseries.model
        delegate: ListItemTimeseries{
            dragParent: root
        }
    }

    MouseArea{
        id: newTimeseriesMouse
        width: root.width
        height: units.gu(8)

        Rectangle{
            anchors.fill: parent
            color: theme.palette.highlighted.positive
            opacity: 0.3
            visible: newTimeseriesMouse.pressed
        }

        Rectangle{
            id: rectNewTS
            anchors.verticalCenter: parent.verticalCenter
            x: units.gu(2)
            width: units.gu(4)
            height: units.gu(4)
            radius: units.gu(1)
            color: theme.palette.normal.positive

            Icon{
                anchors.centerIn: parent
                height: units.gu(2.5)
                name: "add"
                color: theme.palette.normal.positiveText
            }
        }

        Label{
            anchors{
                verticalCenter: parent.verticalCenter
                left: rectNewTS.right
                leftMargin: units.gu(2)
            }
            // TRANSLATORS: time series is singular here
            text: i18n.tr("New time series")
        }

        onClicked: bottomEdgeNewTimeseries.show()
    }
}
