/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import MDTK 1.0
import MDTK.Settings 1.0

ThemedFlickablePage{
    id: root
    title: i18n.tr("Settings")

    SettingsMenuItem{
        text: i18n.tr("Show manual")
        iconName: "info"
        onClicked: manual.open()
    }
    SettingsMenuSwitch{
        text: i18n.tr("Show bottom edge hint")
        checked: settings.showBottomEdgeHint
        onCheckedChanged: settings.showBottomEdgeHint = checked
    }

    SettingsCaption{title: i18n.tr("Appearance")}
    SettingsMenuSwitch{
        text: i18n.tr("Use default theme")
        Component.onCompleted: checked = colors.useDefaultTheme
        onCheckedChanged: colors.useDefaultTheme = checked
    }
    SettingsMenuSwitch{
        enabled: !colors.useDefaultTheme
        text: i18n.tr("Dark Mode")
        onCheckedChanged: colors.darkMode = checked
        Component.onCompleted: checked = colors.darkMode
    }
    SettingsMenuDoubleColorSelect{
        enabled: !colors.useDefaultTheme
        text: i18n.tr("Color")
        model: colors.headerColors
        Component.onCompleted: currentSelectedColor =  colors.currentIndex
        onCurrentSelectedColorChanged: colors.currentIndex = currentSelectedColor
    }
}
