/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * time-series is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import MDTK 1.0

import "../components"

import TSCore 1.0

ThemedPage {
    id: root
    title: ts.title

    readonly property var ts: TSBackend.timeseries.selected

    headerTrailingActionBar.actions: [
        Action{
            iconName: "edit"
            onTriggered: bottomEdgeEditTimeseries.show(ts)
        }
    ]

    TimeseriesMonthSelector{
        id: monthSelector
        anchors.top: header.bottom
        month: ts.getMonthAt(tableArea.lowestVisibleIndex)
        canGoFirst: !tableArea.isAtTop
        canGoLast:  !tableArea.isAtEnd
        onMonthSelected: tableArea.bringIndexToTop(ts.getBeforeCount(month))
        onGoFirst: tableArea.toTop()
        onGoLast: tableArea.toEnd()
    }

    TimeseriesTable{
        id: tableArea
        anchors{
            top: monthSelector.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }
}
