/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * account-book is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3
import MDTK 1.0

import TSCore 1.0

ThemedPage {
    id: root
    title: i18n.dtr("content-hub", "Share to")

    ContentPeerPicker{
        id: contentPeerPicker
        anchors{
            fill: parent
            topMargin: header.height
        }

        showTitle: false
        contentType: ContentType.Documents
        handler: ContentHandler.Share
        onCancelPressed: pageStack.pop()
        onPeerSelected: {
            var curTransfer = peer.request()
            if (curTransfer.state === ContentTransfer.InProgress){
                curTransfer.items = [contentItemComp.createObject(root, {"url": "file:"+ TSBackend.exports.filePath})]
                curTransfer.state = ContentTransfer.Charged
                pageStack.pop()
                pageStack.pop()
            }
        }
    }

    Component {
        id: contentItemComp
        ContentItem {}
    }
}
